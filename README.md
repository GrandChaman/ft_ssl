![pipeline](https://gitlab.com/GrandChaman/ft_ssl/badges/master/build.svg "Build status")
![coverage](https://gitlab.com/GrandChaman/ft_ssl/badges/master/coverage.svg "Code coverage")

# ft_ssl

This project aims to recreate some functions of the OpenSSL program.

## Installation

```sh
make
```

## Testing

```sh
make test

./exec_test.sh
```
