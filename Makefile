# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/01/16 13:28:08 by fle-roy           #+#    #+#              #
#    Updated: 2020/02/27 18:53:47 by fle-roy          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

vpath %.c src
vpath %.c src/hash
vpath %.c src/hash/md5
vpath %.c src/hash/sha1
vpath %.c src/hash/sha2
vpath %.c src/hash/sha2/sha224
vpath %.c src/hash/sha2/sha256
vpath %.c src/hash/sha2/sha384
vpath %.c src/hash/sha2/sha512
vpath %.c src/hash/sha2/sha512_224
vpath %.c src/hash/sha2/sha512_256
vpath %.c src/base64
vpath %.c src/des
vpath %.c tests

SRC_MAIN = main.c

SRC = \
		opts.c \
		sha224_256_op.c \
		sha224_256_op2.c \
		sha224_256.c \
		sha256.c \
		sha224.c \
		sha384_512_op.c \
		sha384_512_op2.c \
		sha384_512.c \
		sha512.c \
		sha384.c \
		sha512_224.c \
		sha512_256.c \
		md5_op.c \
		md5.c \
		sha1.c \
		sha1_op.c \
		read_fd.c \
		hash_utils.c \
		hash_utils_long.c \
		utils.c \
		hash_callback.c \
		hash_to_string.c \
		hash_block.c \
		hash_opts.c \
		hash.c \
		base64.c \
		base64_utils.c \
		base64_encode.c \
		base64_decode.c \
		base64_wrapper.c \
		base64_output.c \
		des_utils.c \
		des_keys.c \
		des_block_op.c \
		des_f.c \
		des_pkdf.c \
		des_encrypt.c \
		des_decrypt.c \
		des_crypt.c \
		des_io.c \
		des_opts_utils.c \
		des_block_cipher.c \
		des.c
INCLUDE= include
OBJ_DIR=obj
DEP_DIR=dep
VERSION_DEFINE := $(shell git describe master --tags)-$(shell \
	git rev-parse --short HEAD)
CFLAGS = -g3 -Wall -Wextra -Werror -I $(INCLUDE) -I ./copt/include \
	-I ./libft/include -DVERSION="\"$(VERSION_DEFINE)\""
CC = clang
LN = clang
TEST_CFLAGS = -I $(INCLUDE) -I ./copt/include -I ./libft/include
TEST_LDFLAGS = -lcriterion
TEST_BIN = ft_ssl_test
TEST_SRC = \
	hash.test.c \
	md5.test.c \
	sha1.test.c \
	sha256.test.c \
	sha224.test.c \
	sha384.test.c \
	sha512.test.c \
	sha512_224.test.c \
	sha512_256.test.c \
	base64.test.c \
	stdin.test.c \
	des.test.c
OBJ = $(SRC:%.c=$(OBJ_DIR)/%.o)
OBJ_MAIN = $(SRC_MAIN:%.c=$(OBJ_DIR)/%.o)
DEP = $(SRC:%.c=$(DEP_DIR)/%.d)
DEP_MAIN = $(SRC_MAIN:%.c=$(DEP_DIR)/%.d)
OBJ_TEST = $(TEST_SRC:%.test.c=$(OBJ_DIR)/%.test.o)
DEP_TEST = $(TEST_SRC:%.test.c=$(DEP_DIR)/%.test.d)
ifeq ($(DEV),true)
	CFLAGS += -g3 -DDEV
	TEST_CFLAGS += -g3 -DDEV
endif

ifeq ($(CODE_COVERAGE),true)
	CFLAGS += -fprofile-arcs -ftest-coverage
	TEST_CFLAGS += -fprofile-arcs -ftest-coverage
	TEST_LDFLAGS += -fprofile-arcs -ftest-coverage
endif
LIBFT = libft/bin/libft.a
COPT = copt/libcopt.a
NAME = ft_ssl
NAME_UP = FT_SSL
all: libft libcopt $(NAME)
test: $(TEST_BIN)
libft:
	@$(MAKE) -C libft
libcopt:
	@$(MAKE) -C copt
$(OBJ_DIR)/%.test.o: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(TEST_CFLAGS) -c $< -o $@
$(DEP_DIR)/%.test.d: %.test.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(TEST_CFLAGS) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(OBJ_DIR)/%.o: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mBuilding $<\033[0m"
	@$(CC) $(CFLAGS) -c $< -o $@
$(DEP_DIR)/%.d: %.c
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mGenerating dependencies - $<\033[0m"
	@$(CC) $(CFLAGS) -MM $^ | sed -e '1s/^/$(OBJ_DIR)\//' > $@
$(TEST_BIN): libft libcopt $(NAME) $(OBJ_TEST)
	@$(CC) $(TEST_CFLAGS) $(OBJ) $(LIBFT) $(TEST_LDFLAGS) $(OBJ_TEST) -o $(TEST_BIN) $(COPT)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mTests done!\033[0m\n"
$(NAME): libft libcopt $(OBJ) $(OBJ_MAIN)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mLinking...\033[0m"
	@$(CC) $(CFLAGS) -o $(NAME) $(OBJ) $(OBJ_MAIN) $(LIBFT) $(COPT)
	@printf "\r\033[K[$(NAME_UP)] \033[1;32mDone!\033[0m\n"
dclean:
	@rm -f $(DEP)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .d!\033[0m\n"
clean: dclean
	@rm -f $(OBJ) $(OBJ_TEST)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .o!\033[0m\n"
fclean: clean
	@rm -f $(NAME) $(TEST_BIN)
	@printf "[$(NAME_UP)] \033[1;31mCleaned .a!\033[0m\n"
re:
	@$(MAKE) -C libft re
	@$(MAKE) -C copt re
	@$(MAKE) fclean
	@$(MAKE) all
-include $(DEP)
-include $(DEP_TEST)
.PHONY: all clean fclean re dclean test libft libcopt
