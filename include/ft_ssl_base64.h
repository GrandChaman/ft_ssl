/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_base64.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 11:12:41 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/02 10:57:01 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_BASE64_H
# define FT_SSL_BASE64_H
# include <stdlib.h>
# include "copt.h"
# include "libft.h"
# include <fcntl.h>
# define B64_OPT_E 0
# define B64_OPT_D 1
# define B64_OPT_I 2
# define B64_OPT_O 3
# define B64_OPT_B 4

extern const char g_b64_encode_table[];
extern const uint8_t g_b64_decode_table[];

typedef struct		s_ft_base64
{
	t_copt_parsed	*copt;
	char			dangling[3];
	unsigned char	dangling_len;
	t_dbuf			obuf;
	int				in_fd;
	int				out_fd;
	size_t			width;
	size_t			cwrote;
}					t_ft_base64;

char				ft_base64_cb(t_copt_parsed *opts);
void				ft_base64_destroy(t_ft_base64 *base64);
void				ft_base64_remove_whitespace(char *buf, size_t *len);
char				ft_base64_encode_cb(t_copt_parsed *opts,
	t_ft_base64 *base64);
void				base64_encode(uint8_t *dest, uint8_t *src, size_t *i);
char				ft_base64_init(t_copt_parsed *opts, t_ft_base64 *b64);
char				ft_base64_encode_update(t_ft_base64 *b64, char *str,
	size_t len);
char				*ft_base64_encode_finish(t_ft_base64 *b64);
char				ft_base64_write_fd(t_ft_base64 *base64);
char				ft_base64_encode_write_fd(t_ft_base64 *b64);
char				*ft_base64_encode(char *str, size_t len);
char				*ft_base64_decode(char *str, size_t len);
char				*ft_base64_decode_finish(t_ft_base64 *base64);
char				ft_base64_decode_update(t_ft_base64 *base64, char *str,
	size_t len);
void				base64_decode(uint8_t *dest, uint8_t *src, size_t *i,
	size_t *j);

#endif
