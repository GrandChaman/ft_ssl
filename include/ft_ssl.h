/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/22 11:08:25 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/27 15:53:36 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_H
# define FT_SSL_H
# include "copt.h"
# include "libft.h"
# include <stdio.h>
# include <sys/errno.h>
# include "ft_ssl_hash.h"
# include "ft_ssl_base64.h"
# include "ft_ssl_des.h"
# define READ_BLOCK 32
# define READ_BLOCK_AL (READ_BLOCK + 16 - (READ_BLOCK % 16))
# ifndef _PASSWORD_LEN
#  include <limits.h>
#  define _PASSWORD_LEN _SC_PASS_MAX
# endif
# if READ_BLOCK_AL < 32
#  error READ_BLOCK_AL is not large enough
# endif
# if BUFF_SIZE < 8
#  error BUFF_SIZE is not large enough
# endif
# ifndef VERSION
#  define VERSION "unkown_version-000"
# endif
# define RANDOM_DEVICE "/dev/random"
# define FT_READ_ECHO			0b00000001
# define FT_READ_SINGLE_ROUND	0b00000010
# define FT_READ_ALIGNED		0b00000100

char		ft_ssl_global_cmd(t_copt_parsed *parsed);
char		ft_ssl_error(t_copt_parsed *parsed, char *file);
char		ft_ssl_error_spec(t_copt_parsed *parsed, char *msg);
int			ft_ssl_open_input_stream(t_copt_parsed *opts, size_t pos);
int			ft_ssl_open_output_stream(t_copt_parsed *opts, size_t pos);
char		*ft_ssl_get_pass(uint8_t repeat);
char		ft_read_fd(int fd, void *param,
	char (*update)(void*, char*, size_t), uint8_t opt);
char		is_be(void);
uint64_t	ft_pad_hex(char *str);

extern t_copt	g_copt;

#endif
