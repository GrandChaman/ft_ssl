/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_des.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 11:12:41 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/02 10:55:39 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_DES_H
# define FT_SSL_DES_H
# include <stdlib.h>
# include "copt.h"
# include "ft_ssl_base64.h"
# include "libft.h"
# include <fcntl.h>
# define DES_OPT_E 0
# define DES_OPT_D 1
# define DES_OPT_I 2
# define DES_OPT_O 3
# define DES_OPT_B 4
# define DES_OPT_A 5
# define DES_OPT_K 6
# define DES_OPT_P 7
# define DES_OPT_S 8
# define DES_OPT_V 9
# define SALTED_STRING "Salted__"
# define SALTED_STRING_LEN 8
# define DES_IV		0b001
# define DES_NO_PAD	0b010
# define DES_SYM	0b100

typedef struct		s_ft_couple_num
{
	t_ft_num		left;
	t_ft_num		right;
}					t_ft_couple_num;

typedef struct		s_ft_des
{
	t_ft_base64		base64;
	t_dbuf			*ibuf;
	t_dbuf			*obuf;
	uint8_t			armored;
	uint64_t		size;
	t_ft_num		last;
	t_ft_num		iv;
	t_ft_num		key;
	t_ft_num		skey[16];
	t_ft_num		salt;
	int				fd_in;
	int				fd_out;
	char			mode;
	char			dangling[8];
	uint8_t			dangling_len;
	uint8_t			cipher_opt;
	void			(*routine)(struct s_ft_des*, char*, char*);
}					t_ft_des;

typedef struct		s_ft_des_block_cipher_mode
{
	char			*title;
	void			(*routine)(t_ft_des*, char*, char*);
	char			options;
}					t_ft_des_block_cipher_mode;

void				ft_des_destroy(t_ft_des *des);
t_ft_num			ft_des_str_to_key(char *key);
t_ft_num			ft_des_str_to_block(char *str, size_t len);
void				ft_des_get_subkeys(t_ft_num key, t_ft_num res[16]);
void				ft_des_split_64(t_ft_num input, t_ft_couple_num *res);
void				ft_des_split_56(t_ft_num input, t_ft_couple_num *res);
void				ft_des_split_48(t_ft_num input, t_ft_couple_num *res);
void				ft_des_ip(t_ft_num *block);
void				ft_des_fp(t_ft_num *block);
void				ft_des_f_e(t_ft_num *block);
void				ft_des_f_xor(t_ft_num *block, t_ft_num kn);
void				ft_des_f_sbox(t_ft_num *block);
void				ft_des_f_p(t_ft_num *block);
void				ft_des_f(t_ft_num *block, t_ft_num kn);
void				ft_des_process_block(t_ft_num *block, t_ft_num k[16],
	uint8_t enc);
char				ft_des_cb(t_copt_parsed *opts);
void				ft_des_pkdf(t_ft_des *des, char *pass, char *salt);
char				*ft_des_get_salt(void);
char				ft_des_print_salt(t_ft_des *des);
void				ft_des_decrypt_handle_padding(t_ft_des *des);
char				ft_des_routine(t_ft_des *des, uint8_t last);
char				ft_des_finish(t_ft_des *des);
void				ft_des_routine_ecb(t_ft_des *des, char *data, char *buf);
void				ft_des_routine_cbc(t_ft_des *des, char *data, char *buf);
void				ft_des_routine_ofb(t_ft_des *des, char *data, char *buf);
void				ft_des_routine_cfb(t_ft_des *des, char *data, char *buf);
char				ft_des_print(t_ft_des *des, uint8_t last);
void				ft_des_read_armor(t_ft_des *des, char *buf, size_t len);
char				ft_des_read_salt(t_ft_des *des, char *buf, size_t len);
char				ft_des_io_routine(t_ft_des *des, char *buf, size_t len);
char				ft_des_finish_enc(t_ft_des *des);
char				ft_des_check_hex_long(size_t nb, char **args);
char				ft_des_check_hex(size_t nb, char **args);

#endif
