/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ssl_hash.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 11:43:15 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/26 11:26:14 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SSL_HASH_H
# define FT_SSL_HASH_H
# include <stdlib.h>
# include "copt.h"
# include "libft.h"
# include <fcntl.h>
# include <unistd.h>

# define MD5_REG_NB 4
# define SHA1_REG_NB 5
# define SHA2_REG_NB 8
# define SHA3_REG_NB 8

# define HT_MD5 0
# define HT_SHA1 1
# define HT_SHA224 2
# define HT_SHA256 3
# define HT_SHA384 4
# define HT_SHA512 5
# define HT_SHA512224 6
# define HT_SHA512256 7

# define REG_A 0
# define REG_B 1
# define REG_C 2
# define REG_D 3
# define REG_E 4
# define REG_F 5
# define REG_G 6
# define REG_H 7

# define GB_OPT_V 0
# define GB_OPT_H 1

# define HT_OPT_P 0
# define HT_OPT_Q 1
# define HT_OPT_R 2
# define HT_OPT_S 3

# define HASH_RAW 0b01

# define MAX_HASH_INTERNAL_SIZE 128

typedef union		u_hash_block
{
	uint8_t			blocks[MAX_HASH_INTERNAL_SIZE];
	uint32_t		words[MAX_HASH_INTERNAL_SIZE / 4];
	uint64_t		bwords[MAX_HASH_INTERNAL_SIZE / 8];
}					t_hash_block;

struct s_ft_hash_ctx;
typedef struct		s_ft_hash
{
	char			*title;
	uint8_t			reg_nb;
	uint32_t		block_size;
	uint32_t		len_size;
	uint32_t		out_len;
	void			(*init)(struct s_ft_hash_ctx*);
	void			(*update)(struct s_ft_hash_ctx*, t_hash_block hb);
	char			*(*finish)(struct s_ft_hash_ctx*, t_hash_block hb);
}					t_ft_hash;

typedef struct		s_ft_hash_ctx
{
	t_ft_num		*registers;
	uint8_t			*buf;
	uint16_t		cursor;
	t_ft_num		total_size;
	t_ft_hash		hash;
	uint8_t			opt;
}					t_ft_hash_ctx;

typedef struct		s_ft_md5_round
{
	uint32_t		(*f)(uint32_t, uint32_t, uint32_t);
	u_int8_t		k;
	uint32_t		sin;
	char			rot;
}					t_ft_md5_round;

extern t_ft_hash	g_hash_table[];
extern char			g_be;
extern uint32_t		g_sha2_k[];
extern uint64_t		g_sha3_k[];

/*
** MD5
*/

uint32_t			md5_op_f(uint32_t x, uint32_t y, uint32_t z);
uint32_t			md5_op_g(uint32_t x, uint32_t y, uint32_t z);
uint32_t			md5_op_h(uint32_t x, uint32_t y, uint32_t z);
uint32_t			md5_op_i(uint32_t x, uint32_t y, uint32_t z);
void				ft_md5_init(t_ft_hash_ctx *ctx);
void				ft_md5_update(t_ft_hash_ctx *ctx, t_hash_block hb);
char				*ft_md5_finish(t_ft_hash_ctx *ctx, t_hash_block hb);

/*
** UTILS x32
*/

uint32_t			left_rot(uint32_t x, unsigned char c);
uint32_t			right_rot(uint32_t x, unsigned char c);
uint32_t			brev_32(uint32_t b);
void				add_reg(t_ft_hash_ctx *ctx, uint32_t *reg2);
void				ft_hash_copy_reg(t_ft_hash_ctx *ctx, uint32_t *reg);
char				*ft_hash_to_string(t_ft_hash_ctx *ctx, uint8_t rev,
	uint8_t reg_nb);

/*
** UTILS x64
*/

uint64_t			brev_64(uint64_t b);
uint64_t			left_rot_long(uint64_t x, unsigned char c);
uint64_t			right_rot_long(uint64_t x, unsigned char c);
void				add_reg_long(t_ft_hash_ctx *ctx, uint64_t *reg2);
void				ft_hash_copy_reg_long(t_ft_hash_ctx *ctx, uint64_t *reg);
char				*ft_hash_to_string_long(t_ft_hash_ctx *ctx,
	uint8_t rev, uint8_t reg_nb);

/*
** FT_HASH
*/

void				ft_hash_init(t_ft_hash_ctx *ctx);
char				ft_hash_start(t_ft_hash has, t_copt_parsed *opts);
char				ft_hash_select(t_copt_parsed *opts);
char				ft_hash_block_update(t_ft_hash_ctx *ctx,
	unsigned char *data, size_t size);
char				*ft_hash_block_finish(t_ft_hash_ctx *ctx);
void				ft_hash_destroy(t_ft_hash_ctx *ctx);
void				ft_hash_print(t_copt_parsed *opts, t_ft_hash_ctx *ctx,
	char *str, char is_file);
char				ft_hash_files(t_ft_hash_ctx *ctx, t_copt_parsed *opts);
char				ft_hash_strings(t_ft_hash_ctx *ctx, t_copt_parsed *res,
	t_copt_llist *llist);
char				*ft_hash_exec(size_t hash, char *str, size_t len);

/*
** SHA1
*/

uint32_t			sha1_op(uint32_t i, uint32_t x, uint32_t y, uint32_t z);
uint32_t			sha1_const(uint32_t i);
void				ft_sha1_init(t_ft_hash_ctx *ctx);
void				ft_sha1_update(t_ft_hash_ctx *ctx, t_hash_block hb);
char				*ft_sha1_finish(t_ft_hash_ctx *ctx, t_hash_block hb);

/*
** SHA2
*/

uint32_t			sha2_op_ch(uint32_t x, uint32_t y, uint32_t z);
uint32_t			sha2_op_maj(uint32_t x, uint32_t y, uint32_t z);
uint32_t			sha2_op_bsig0(uint32_t x);
uint32_t			sha2_op_bsig1(uint32_t x);
uint32_t			sha2_op_ssig0(uint32_t x);
uint32_t			sha2_op_ssig1(uint32_t x);
void				ft_sha2_update(t_ft_hash_ctx *ctx, t_hash_block hb);

/*
** SHA224
*/

void				ft_sha224_init(t_ft_hash_ctx *ctx);
char				*ft_sha224_finish(t_ft_hash_ctx *ctx, t_hash_block hb);

/*
** SHA256
*/

void				ft_sha256_init(t_ft_hash_ctx *ctx);
char				*ft_sha256_finish(t_ft_hash_ctx *ctx, t_hash_block hb);

/*
** SHA3
*/

uint64_t			sha3_op_ch(uint64_t x, uint64_t y, uint64_t z);
uint64_t			sha3_op_maj(uint64_t x, uint64_t y, uint64_t z);
uint64_t			sha3_op_bsig0(uint64_t x);
uint64_t			sha3_op_bsig1(uint64_t x);
uint64_t			sha3_op_ssig0(uint64_t x);
uint64_t			sha3_op_ssig1(uint64_t x);
void				ft_sha3_update(t_ft_hash_ctx *ctx, t_hash_block hb);

/*
** SHA384
*/

void				ft_sha384_init(t_ft_hash_ctx *ctx);
char				*ft_sha384_finish(t_ft_hash_ctx *ctx, t_hash_block hb);

/*
** SHA512
*/

void				ft_sha512_init(t_ft_hash_ctx *ctx);
char				*ft_sha512_finish(t_ft_hash_ctx *ctx, t_hash_block hb);

/*
** SHA512224
*/

void				ft_sha512224_init(t_ft_hash_ctx *ctx);
char				*ft_sha512224_finish(t_ft_hash_ctx *ctx, t_hash_block hb);

/*
** SHA512256
*/

void				ft_sha512256_init(t_ft_hash_ctx *ctx);
char				*ft_sha512256_finish(t_ft_hash_ctx *ctx, t_hash_block hb);

#endif
