#!/bin/bash
MD5_1="md5"
MD5_2="./ft_ssl md5"
COL=$(tput cols)
RES_POS=$(($COL - 4))
MOVE_TO_COL="printf \\033[${RES_POS}G"
# GREEN="\033[92m"
# RED="\033[91m"
# DEFAULT="\033[39m"
# CYAN="\033[36m"
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
RES=0

test_md5 ()
{
	printf "MD5 ==>> %s" "$1"
	if [ -z "$3" ]
	then
		diff <(eval ${MD5_1} $2) <(eval ${MD5_2} $2)
	else
		diff <(eval $3 "$4" | eval ${MD5_1} $2) <(eval $3 "$4" | eval ${MD5_2} $2)
	fi
	if [ $? -eq 0 ]
	then
		$MOVE_TO_COL
		printf "%s" "${GREEN}[OK]${NORMAL}" $'\n'
	else
		$MOVE_TO_COL
		printf "%s" "${RED}[KO]${NORMAL}" $'\n'
		RES=1
	fi
}

test_md5 'Simple string 0' '-s Hello'
test_md5 'Simple string 1' '-s World'
test_md5 'Simple string 2' '-s Coucou'
test_md5 'Simple string 3' '-s "Ca va"'
test_md5 'Simple string 4' '-s Oui'
test_md5 'Simple string 5' '-s Et'
test_md5 'Simple string 6' '-s Toi'
test_md5 'Empty string'    '-s ""'

test_md5 'Multiple string 0' '-s Salut -s "Ca va"'
test_md5 'Multiple string 1' '-s Ouais -s wefwefwef'
test_md5 'Multiple string 2' '-s Sal223rut -s gererg'
test_md5 'Multiple string 3' '-s eeeeeee -s 135456'
test_md5 'Multiple string 4' '-s pppppp -s pppppp -s pppppp -s pppppp -s pppppp -s pppppp'

test_md5 'Reverse string 0' '-rs bb26194c-8f1f-58f7-b41f-328c8676efae'
test_md5 'Reverse string 1' '-rs cafc4fa4-6850-5d69-98c4-77c55c71752b'
test_md5 'Reverse string 2' '-rs 7e5cbdb0-f0f9-51ff-b7d0-7fe3f91359b2'
test_md5 'Reverse string 3' '-rs 96c90b0b-8ce0-501e-b152-f0321c69bc23'
test_md5 'Reverse string 4' '-rs 0a06fadf-baec-5926-ac48-f73d7db499b6'
test_md5 'Reverse string 5' '-rs 2d676a5a-a670-5133-ad23-215dbe966358'
test_md5 'Reverse string 6' '-rs 4c45b747-de49-50e0-af54-383e5a225114'
test_md5 'Reverse string 7' '-rs wefwefwefwefwefwefwefwefwefwejnkjwfkjhfwejbkwefbjwekjbfw'
test_md5 'Reverse string 8' '-rs wefwefwefwefwefwefwefwefwefwejnkjwfkjhfwejbkwefbjwekjbfwe'
test_md5 'Reverse string 8' '-rs wefwefwefwefwefwefwefwefwefwejnkjwfkjhfwejbkwefbjwekjbfweeeeeeee'

test_md5 'Reverse multiple string 0' '-r -s cifut@digjete.sy -s conenel@je.in'
test_md5 'Reverse multiple string 1' '-r -s ginona@naju.br -s orlib@fow.tm'
test_md5 'Reverse multiple string 2' '-r -s afumwa@reh.zw -s umicek@ufajube.dk'
test_md5 'Reverse multiple string 3' '-r -s inpa@zita.tt -s umrudad@ubuvupres.hk'
test_md5 'Reverse multiple string 4' '-r -s onovido@waviz.ve -s fikigfid@nirwinav.mz'
test_md5 'Reverse multiple string 5' '-r -s rotsefo@ib.nl -s cujwauj@epimig.bv'
test_md5 'Reverse multiple string 6' '-r -s oduemaam@lar.cm -s ugeesoba@hivwu.gl'
test_md5 'Reverse multiple string 7' '-r -s uzho@milub.gw -s jigih@mek.lc'


test_md5 'Quiet string 0' '-q -s fogwo@tab.ax'
test_md5 'Quiet string 1' '-q -s be@gibvihpo.na'
test_md5 'Quiet string 2' '-q -s we@jokiwu.io'
test_md5 'Quiet string 3' '-q -s wev@ate.la'
test_md5 'Quiet string 4' '-q -s kot@iwed.ge'
test_md5 'Quiet string 5' '-q -s rute@hidfaref.gq'
test_md5 'Quiet string 6' '-q -s ku@toras.am'
test_md5 'Quiet string 7' '-q -s uf@mappocuz.ml'

test_md5 'Quiet multiple string 0' '-q -s ohodutrum@ju.pk -s vumemwu@atogesu.bv'
test_md5 'Quiet multiple string 1' '-q -s li@inri.je -s jiv@wezejo.su'
test_md5 'Quiet multiple string 2' '-q -s arisa@gec.pn -s huavfo@vumjuc.mw'
test_md5 'Quiet multiple string 3' '-q -s ehpudo@ti.bz -s hocume@ev.ls'
test_md5 'Quiet multiple string 4' '-q -s dok@oferamas.sn -s emli@fo.sk'
test_md5 'Quiet multiple string 5' '-q -s het@togpu.tt -s ufwore@uwekti.ac'
test_md5 'Quiet multiple string 6' '-q -s worcimob@tutow.no -s siwu@ge.bz'
test_md5 'Quiet multiple string 7' '-q -s vebbi@hunru.gi -s teweho@rako.ky'

test_md5 "Stdin 0" "-p" "echo" "Salut Salut"
test_md5 "Stdin 1" "-p" "echo" "Comment va mon gros"
test_md5 "Stdin 2" "-p" "echo" "Cest le feu copain"
test_md5 "Stdin 3" "-p" "echo" "Ouais de OUF"

test_md5 "File 0" "-p" "cat" "Makefile"
test_md5 "File 1" "-p" "cat" ".gitignore"
test_md5 "File 2" "-p" "cat" "author"

exit ${RES}