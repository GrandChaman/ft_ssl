/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224.test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/10 13:00:20 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_sha224_test(char *str, char *res_verif)
{
	t_ft_hash_ctx 	ctx;
	char	*res;

	g_be = is_be();
	ctx.hash = g_hash_table[HT_SHA224];
	ft_sha224_init(&ctx);
	ft_hash_block_update(&ctx, (unsigned char*)str,
		ft_strlen((const char*)str));
	res = ft_hash_block_finish(&ctx);
	cr_assert(!strcmp(res, (const char*)res_verif));
	free(res);
}

Test(SHA224, Simple)
{
	routine_sha224_test("a", "abd37534c7d9a2efb9465de931cd7055ffdb8879563ae98078d6d6d5");
	routine_sha224_test("ab", "db3cda86d4429a1d39c148989566b38f7bda0156296bd364ba2f878b");
}

Test(SHA224, BigUnder512)
{
	routine_sha224_test("hoKgmq3kcZXGR0jNbh+EPI2fIrcKZlDszuVPATUd0Ds=",
		"f1150e1d9f561cf9add960386efd765dfa28e7be4f23bd5a579a3b35");
	routine_sha224_test("yuEWkKBs0Uf7tv8FwsnEDC+zomXpbGkjHBB0OvsTufdyrxZp9V04vO",
		"46a890e0956d8bd148e6b53fc4277bc3371c83ea46b9bf3eb345ac03");
	routine_sha224_test("7U96W6jnIpgiowEIbK3270sYj/uFoqzpJSmhKyeK+bEz3u+JtiOuK5",
		"25a342c7c77cb6636eb66cd4ee084a94a10d7f27535c9a47bd3db572");
}

Test(SHA224, BigAbove512)
{
	routine_sha224_test("yf1lmoS8GvquIiD8FhwJyVM4bKTOyAb5zlggQU4Anf0DM6XCjxxHZFsIoK3XDZR4xhzZoXL9AvXxJ2IfTZzc9iGGPQvn+4udLzDM/iOhFY0ENxR4PqCZnclxdBNxWXf2rwwo4wxZuamU96yPrhHIxzy7B7KO3AMYfjNeJboKnyZr2crISc35vhZMp/Aa4/dDlHb+4FWD+q+6vuOqlNXXyBSTa9vcuuKhWerx34M/E0qRiEQpiSTEAiUX5dHj7AKL67NHZAXMNDUD/ncN0FCqmbbWVfls68sQoSTrMbDOKcSPgKKjRgYFr66YrcwfxaDgfwrJZD6TvA76eh7SbBIv+/T1Pt4KmkR3T9kMQrm8pU9oUOxV/yXrrIz8B9S5RqK5Ubx3A3RPraO1DfIQUUEOSXSr+MJl8TdmzZLj6VX7zN4MQC2yc1kfPciF3LAiqHucBM95A9msUEzrci3MVuxggw+s4KiFi+Dic0Bqv+rIGgZDy7D/eDrUyXm4h5U2XPQgoJGBEmycjvp0CU45gWi1Gbxo6oCLa9jER3hrCH7wVzxdjTnDZG8i6plBx5cS5XWHd5OjIiHO1gcpm26NDj6MMlw9xW6jnqv10DQLFDroRbCOjmXeEpZoJ5s70lf9TDEbh4jz7pvOf1jUeptqnl4XQ7McORYODCuLior1Osbgo9Ds09KR6iCoj5sVcMxd25F3/cXusHzvaZn9oQm9l+lZ5RIYo4JXNC8EVHfyklpSyfeuEhUEU4Q2lo4pHQnfojFz5p2zroLVc5NVDr4mXL9yB7EFXR1hkCK93F+AxZK2cd4Fs9H38jW0huxAw9XghRJS6Bj1Ai18YRUENI6zYFTEs3f3+UJcAH9/QFqnC3SZctDOrI4jaqD5dI9NuKijJUkd0HY9wQEhUkp0+cYyJOXeT4MZ0wivryfTvAYZh+zi5+DjET8xwF/cuWc8QMuXKYxu4uqyDs/JgIrltrXo/cnzitgr+ZUqTfJ7nOdsWO/kiq5GJLWe/KIx8pp23Xxpg2Al5f/4yuG5MjwD24MvoWSduH2U5/dr15ckGYCguSX93ThWVSWUOwzZ6P62VRxPFJD7XSr9HdsMPhK58dMIGs1NXH2KIFTKJSkCqYVdBtWGFCXZNF4cgfQr6t+7FwCrTlpW/VZZcde0X+GzL6joVDYLihW8zRytxvwvESyQ6nETkE+kg8h/q02bMLBfmWyUjoPesskVWpBqt5wkfv5lqgo5jyUXhtE57aVgE9HoRYD0PzJzQGED4BCR6EhFx/rBYcD6BFd/av1M8jnt+wmwGHUM66UksEGEaZG7w34zvGV7h5hc8RVZcu5t1A==",
		"1a8070e8d0ba60b24b348389d54091947dacf368de5f5fe0d3d48017");
}