/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/11 17:09:12 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_hash_test(size_t mode, char *str, char *res_verif)
{
	t_ft_hash_ctx 	ctx;
	char			*res;

	g_be = is_be();
	res = ft_hash_exec(mode, str, 0);
	cr_assert_str_eq(res, res_verif);
	free(res);
}

Test(Hash, MD5)
{
	routine_hash_test(HT_MD5, "a", "0cc175b9c0f1b6a831c399e269772661");
}

Test(Hash, SHA1)
{
	routine_hash_test(HT_SHA1, "a", "86f7e437faa5a7fce15d1ddcb9eaeaea377667b8");
}

Test(Hash, SHA256)
{
	routine_hash_test(HT_SHA256, "a", "ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b9807785afee48bb");
}