#!/bin/bash
SHA512_1="shasum -a 512 | cut -d' ' -f 1 "
SHA512_2="./ft_ssl sha512"
COL=$(tput cols)
RES_POS=$(($COL - 4))
MOVE_TO_COL="printf \\033[${RES_POS}G"
# GREEN="\033[92m"
# RED="\033[91m"
# DEFAULT="\033[39m"
# CYAN="\033[36m"
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
RES=0

test_sha512 ()
{
	printf "SHA512 ==>> %s" "$1"
	if [ -z "$3" ]
	then
		diff <(eval ${SHA512_1} $2) <(eval ${SHA512_2} $2)
	else
		diff <(eval $3 "$4" | eval ${SHA512_1} $2) <(eval $3 "$4" | eval ${SHA512_2} $2)
	fi
	if [ $? -eq 0 ]
	then
		$MOVE_TO_COL
		printf "%s" "${GREEN}[OK]${NORMAL}" $'\n'
	else
		$MOVE_TO_COL
		printf "%s" "${RED}[KO]${NORMAL}" $'\n'
		RES=1
	fi
}

test_sha512 "Stdin 0" "" "echo" "Salut Salut"
test_sha512 "Stdin 1" "" "echo" "Comment va mon gros"
test_sha512 "Stdin 2" "" "echo" "Cest le feu copain"
test_sha512 "Stdin 3" "" "echo" "Ouais de OUF"

test_sha512 "File 0" "" "cat" "Makefile"
test_sha512 "File 1" "" "cat" ".gitignore"
test_sha512 "File 2" "" "cat" "author"

exit ${RES}