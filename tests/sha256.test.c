/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256.test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/10 13:00:20 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_sha256_test(char *str, char *res_verif)
{
	t_ft_hash_ctx 	ctx;
	char	*res;

	g_be = is_be();
	ctx.hash = g_hash_table[HT_SHA256];
	ft_sha256_init(&ctx);
	ft_hash_block_update(&ctx, (unsigned char*)str,
		ft_strlen((const char*)str));
	res = ft_hash_block_finish(&ctx);
	cr_assert(!strcmp(res, (const char*)res_verif));
	free(res);
}

Test(SHA256, Simple)
{
	routine_sha256_test("a", "ca978112ca1bbdcafac231b39a23dc4da786eff8147c4e72b"
		"9807785afee48bb");
	routine_sha256_test("ab", "fb8e20fc2e4c3f248c60c39bd652f3c1347298bb977b8b4d"
		"5903b85055620603");
}

Test(SHA256, BigUnder512)
{
	routine_sha256_test("hoKgmq3kcZXGR0jNbh+EPI2fIrcKZlDszuVPATUd0Ds=",
		"ae3ef7974cc5e871ca213115ed064916609ee821e2634185cb45e699586b9423");
	routine_sha256_test("yuEWkKBs0Uf7tv8FwsnEDC+zomXpbGkjHBB0OvsTufdyrxZp9V04vO"
		"5IBqoa",
		"a52f648a5210d2c132a3957885468c65b6ff1b86aab796ea900f2fe50debd136");
	routine_sha256_test("7U96W6jnIpgiowEIbK3270sYj/uFoqzpJSmhKyeK+bEz3u+JtiOuK5"
		"PY",
		"95f2f5d49b753b298ffbc3ef92a0412d95563a5e552fbd8c059e1d73f3861983");
}

Test(SHA256, BigAbove512)
{
	routine_sha256_test("KCyAkTPiyKiVqwRk1nN0yQROqbmIhkFdmt7fGjQRVbmTNhcjlmgFa/"
	"duiR6WInHwwB0yPUPcfurerUD66iBXZLCsTtWXVreu/EYKCDrvF9W6PKngXN9g3kkA1UrX73I6"
	"KhNEDpBk7HwNEv7zRTbb7tOAjJ4dXUSVeL2MZQva2icsSzWp4lV5ktmv6Ow3ycirfQzv/7/p8I"
	"TvSArJ8CwuswF6VQPfSp75bAtFTvkp0p1Aj3ka0hLgmgxewsaDqKuqgtfNbVThdNfsLHPE3feh"
	"5vY1TAe0Z5eOCjLl+30G3Fj/ySdeFbOtDsxOPZqK0GvxSEQDuoWlu7MLb/r1MrhXk9kZzAjCyr"
	"XLjkw3FHi3jCHYa3+KXOmHXVnBglEIuaXVgzHv3W7PEKIpvYOH332t8MzKi5zK6yRhYr0LpUvC"
	"yjRZ5MppFHpSV1oOhiz6k6rsm0v5HZNe6iRrzss70K7/dCXED+thszmlMgX3Yd+UwxaVTDdyub"
	"P1cKakqN359JwqrYirtMskaI6mM0XqoEDkQvI3JCV+lG4U5qlnVkt509Wgz7fU0LbV4462ToKO"
	"C/AVq/JV2thI2rSzGFSzns0x2Th2XNjp8KypWn06T+oRXLJyq+GL0G5eDt5UBd9gigyO6F1xYQ"
	"+5VWPKLgr7ESYOlJ2lCirzrQcBMgMlFTZWgxTKm24SnGAVUqLmMaWOU0npAWH0DsQmasAWgoRr"
	"u6Xqm30Kwoulq5FzJ6oA48F0nappvpKBCbeaoSsBHkJjfAdb3p7A8YS0ASl3Uj+Dieoyb/a1/V"
	"0q9lc/3HO3kfUkl738L19mGtYq9beHRPZy5LZNOpqLK3kjDQpQpU/ItPUn4co/V/e1APui1bc2"
	"HTlTAHF8NfOCKoeGpUCdEEFzOcdo0R/p86eqPHNYOjyYmMfIsxItsYjn5qQrtCYg23IJBQNoB8"
	"Ujia3JxgIFLy5dAzn6t2HojYnpXXJdXo+t3SrlZU7czbpAB+QiZ8/u6VVUrDg/ZGcKR2LTEJtg"
	"chqEI8b1593QO73TZMXqovT9XHLyOyB9E6gfZaib4cY22CQXa8Z+npmoerEhakt2tbVIcBaiww"
	"J98j8ckKKwpPnOolDjaNsHq/KgHbhaeOkr4EMmwVvRhPkGAdZQ5MMmeHAQDrMHMX9VQvWV+yhh"
	"16brpGOgt0564HxKFW3cf2KIcFjr7YZr2AjnJUDgChLOp3N9PGRF0P/TVoCNrgcvjXnEQMLtnb"
	"XWlS7CRyywO9zCS0SULd8/bfrjpqyotfpYOFSKnXO38Mj2Lmr+Q+bAT6D6H2QGoxK/yOjQjlCA"
	"uk6NGOUolHKvDvYdwpS7Sw==",
		"7b01c6f893bdfbacee4d6bdc8465f34185d5e29e300261aeef71908a0fa20f4f");
}