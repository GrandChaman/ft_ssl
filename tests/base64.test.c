/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64.test.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/09 18:04:54 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_base64_encode_test(char *str, char *res_verif)
{
	char	*res;

	res = ft_base64_encode(str, 0);
	cr_assert(!strcmp(res, (const char*)res_verif));
	free(res);
}

void	routine_base64_decode_test(char *str, char *res_verif)
{
	char	*res;

	res = ft_base64_decode(str, 0);
	cr_assert(!strcmp(res, (const char*)res_verif));
	free(res);
}

Test(BASE64, Simple_Encode)
{
	routine_base64_encode_test("a", "YQ==");
	routine_base64_encode_test("ab", "YWI=");
	routine_base64_encode_test("abc", "YWJj");
	routine_base64_encode_test("abcd", "YWJjZA==");
}

Test(BASE64, BigUnder512_Encode)
{
	routine_base64_encode_test("hoKgmq3kcZXGR0jNbh+EPI2fIrcKZlDszuVPATUd0Ds=",
		"aG9LZ21xM2tjWlhHUjBqTmJoK0VQSTJmSXJjS1psRHN6dVZQQVRVZDBEcz0=");
	routine_base64_encode_test("yuEWkKBs0Uf7tv8FwsnEDC+zomXpbGkjHBB0OvsTufdyrxZp9V04vO5IBqoa",
		"eXVFV2tLQnMwVWY3dHY4RndzbkVEQyt6b21YcGJHa2pIQkIwT3ZzVHVmZHlyeFpwOVYwNHZPNUlCcW9h");
	routine_base64_encode_test("7U96W6jnIpgiowEIbK3270sYj/uFoqzpJSmhKyeK+bEz3u+JtiOuK5PY",
		"N1U5Nlc2am5JcGdpb3dFSWJLMzI3MHNZai91Rm9xenBKU21oS3llSytiRXozdStKdGlPdUs1UFk=");
}

Test(BASE64, BigAbove512_Encode)
{
	routine_base64_encode_test("8LQ3qiH5fFoSagDveXbH/7aI6kuqqfaGAJ+jo/FmCsQBpPeTw6EBEj7zZGkt02D37ekW/BOtBZDY05v30VhfWWLAxH1NVQ7xeA5+3hRmoDWSd4aoHTGdjcGdtDAIj6YIDs6tpjp3gi+T/E0ntciJTLBXLyRtd4RiDzGjwSvTCgUJzlPdF80tTe8OOAnoYB1eKHiro04ZOzp+zKjJ4W0ibG2LH1jcfLhxEdFaLd2Lt2yxQqIvan6XB8OVjHmZczHZhl3GEkX4vbOakoOmvAEYR0smgAOev1BNWFCeycyh+gmkl9Nrjy0jSY8nLekoYtp5lGRtBEMfLtrgLvU57xCV9XX7njkbd9FRVzdRXYd1FUe2Mj5sj4ARcZjwtyaKDgeQd4PRxa66SaX6+DWw/F5WZW0CtuniRgVFHFeWgi1n1mLnG53KKvxaUvVemnxJ6sw65x23XDK9zYYjRHU+zeeSBrYORo2ffzaDcu539DDoBGLqXOCWvIbciR0xB8/jn2CMZH+g10BAc5fvNrvg0iPJ6iOn184ashqgW2gHLa+I2RyRYRKp7CbvV50ThWbYYwnTrPB5+ARWlL5STjjpPLH6OxhUy4UJHhHfQTq9/J6uJIJeNrCjcobTZDFlo0Hb3mY1aGlsJc1CUyaFe0Z6olApMpRR6lC9v4oveSMrx79OkJwziTBKvxucB37fBryZNrOPo8d8IYsFWwXneE0qbVeS/IaGfTGV+Zzu8RneWvhfqyYw5ow7AgvjhMp9p7I0/P5alyNCI5mHyaO3chcRGhRNzuZTZ2lX/2WHsffm7wkXN+TmDryh6/FYWelkP1p/qOeqonWdavXoSswlzgXuMfKf6K2JhObHLml2j/sbdbo4yj59uj5ThSYmbSxi4xQX4CMjKqyAXLrxH0Mqf8rgMOwpzv3E8TD136YY+cS1CXQaIB54PXMCDHCkWUF2hJPZymQcrV5RRHyMz+iWQTFp8nfIz8IaJKLlD3lpLvdK9Q3NVrhYs03oGY+loA6t8JB5MX9PxTI4tqOZraJLe8D0+FlK8R6qa6iq7XPL4HpzwXBkZeHodvYTXUeGR89glhhj4VQvjh6VTiFbFkVmNDw0UD3J7HRl9wUTGtdLXvM3EcpBgWPQP/X8F+QMQ9SiKGp2l4yhxfXbUNQuWu3OLB5pKzKk3orP+b86IGJ9p9zIykRzlVyk5Y9wT7+dWHM4G89jvJXTPOa35mYIQdKnyJG6DP49gfBXmj6RVa6P2C/hrmHJM7aLlp7Sn8QaNQvX9Y4+yByEEJm7499SYCO+9w6cVsR52eItd69FV1ceyjpASoeoGvqYofQwTfGGBA==", 
	"OExRM3FpSDVmRm9TYWdEdmVYYkgvN2FJNmt1cXFmYUdBSitqby9GbUNzUUJwUGVUdzZFQkVqN3paR2t0MDJEMzdla1cvQk90QlpEWTA1djMwVmhmV1dMQXhIMU5WUTd4ZUE1KzNoUm1vRFdTZDRhb0hUR2RqY0dkdERBSWo2WUlEczZ0cGpwM2dpK1QvRTBudGNpSlRMQlhMeVJ0ZDRSaUR6R2p3U3ZUQ2dVSnpsUGRGODB0VGU4T09Bbm9ZQjFlS0hpcm8wNFpPenArektqSjRXMGliRzJMSDFqY2ZMaHhFZEZhTGQyTHQyeXhRcUl2YW42WEI4T1ZqSG1aY3pIWmhsM0dFa1g0dmJPYWtvT212QUVZUjBzbWdBT2V2MUJOV0ZDZXljeWgrZ21rbDlOcmp5MGpTWThuTGVrb1l0cDVsR1J0QkVNZkx0cmdMdlU1N3hDVjlYWDduamtiZDlGUlZ6ZFJYWWQxRlVlMk1qNXNqNEFSY1pqd3R5YUtEZ2VRZDRQUnhhNjZTYVg2K0RXdy9GNVdaVzBDdHVuaVJnVkZIRmVXZ2kxbjFtTG5HNTNLS3Z4YVV2VmVtbnhKNnN3NjV4MjNYREs5ellZalJIVSt6ZWVTQnJZT1JvMmZmemFEY3U1MzlERG9CR0xxWE9DV3ZJYmNpUjB4Qjgvam4yQ01aSCtnMTBCQWM1ZnZOcnZnMGlQSjZpT24xODRhc2hxZ1cyZ0hMYStJMlJ5UllSS3A3Q2J2VjUwVGhXYllZd25UclBCNStBUldsTDVTVGpqcFBMSDZPeGhVeTRVSkhoSGZRVHE5L0o2dUpJSmVOckNqY29iVFpERmxvMEhiM21ZMWFHbHNKYzFDVXlhRmUwWjZvbEFwTXBSUjZsQzl2NG92ZVNNcng3OU9rSnd6aVRCS3Z4dWNCMzdmQnJ5Wk5yT1BvOGQ4SVlzRld3WG5lRTBxYlZlUy9JYUdmVEdWK1p6dThSbmVXdmhmcXlZdzVvdzdBZ3ZqaE1wOXA3STAvUDVhbHlOQ0k1bUh5YU8zY2hjUkdoUk56dVpUWjJsWC8yV0hzZmZtN3drWE4rVG1EcnloNi9GWVdlbGtQMXAvcU9lcW9uV2RhdlhvU3N3bHpnWHVNZktmNksySmhPYkhMbWwyai9zYmRibzR5ajU5dWo1VGhTWW1iU3hpNHhRWDRDTWpLcXlBWExyeEgwTXFmOHJnTU93cHp2M0U4VEQxMzZZWStjUzFDWFFhSUI1NFBYTUNESENrV1VGMmhKUFp5bVFjclY1UlJIeU16K2lXUVRGcDhuZkl6OElhSktMbEQzbHBMdmRLOVEzTlZyaFlzMDNvR1krbG9BNnQ4SkI1TVg5UHhUSTR0cU9acmFKTGU4RDArRmxLOFI2cWE2aXE3WFBMNEhwendYQmtaZUhvZHZZVFhVZUdSODlnbGhoajRWUXZqaDZWVGlGYkZrVm1ORHcwVUQzSjdIUmw5d1VUR3RkTFh2TTNFY3BCZ1dQUVAvWDhGK1FNUTlTaUtHcDJsNHloeGZYYlVOUXVXdTNPTEI1cEt6S2szb3JQK2I4NklHSjlwOXpJeWtSemxWeWs1WTl3VDcrZFdITTRHODlqdkpYVFBPYTM1bVlJUWRLbnlKRzZEUDQ5Z2ZCWG1qNlJWYTZQMkMvaHJtSEpNN2FMbHA3U244UWFOUXZYOVk0K3lCeUVFSm03NDk5U1lDTys5dzZjVnNSNTJlSXRkNjlGVjFjZXlqcEFTb2VvR3ZxWW9mUXdUZkdHQkE9PQ==");
}

Test(BASE64, Simple_Decode)
{
	routine_base64_decode_test("YQ==", "a");
	routine_base64_decode_test("YWI=", "ab");
	routine_base64_decode_test("YWJj", "abc");
	routine_base64_decode_test("YWJjZA==", "abcd");
}

Test(BASE64, BigUnder512_Decode)
{
	routine_base64_decode_test("aG9LZ21xM2tjWlhHUjBqTmJoK0VQSTJmSXJjS1psRHN6dVZQQVRVZDBEcz0=", "hoKgmq3kcZXGR0jNbh+EPI2fIrcKZlDszuVPATUd0Ds=");
	routine_base64_decode_test("eXVFV2tLQnMwVWY3dHY4RndzbkVEQyt6b21YcGJHa2pIQkIwT3ZzVHVmZHlyeFpwOVYwNHZPNUlCcW9h","yuEWkKBs0Uf7tv8FwsnEDC+zomXpbGkjHBB0OvsTufdyrxZp9V04vO5IBqoa");
	routine_base64_decode_test("N1U5Nlc2am5JcGdpb3dFSWJLMzI3MHNZai91Rm9xenBKU21oS3llSytiRXozdStKdGlPdUs1UFk=", "7U96W6jnIpgiowEIbK3270sYj/uFoqzpJSmhKyeK+bEz3u+JtiOuK5PY");
}

Test(BASE64, BigAbove512_Decode)
{
	routine_base64_decode_test("OExRM3FpSDVmRm9TYWdEdmVYYkgvN2FJNmt1cXFmYUdBSitqby9GbUNzUUJwUGVUdzZFQkVqN3paR2t0MDJEMzdla1cvQk90QlpEWTA1djMwVmhmV1dMQXhIMU5WUTd4ZUE1KzNoUm1vRFdTZDRhb0hUR2RqY0dkdERBSWo2WUlEczZ0cGpwM2dpK1QvRTBudGNpSlRMQlhMeVJ0ZDRSaUR6R2p3U3ZUQ2dVSnpsUGRGODB0VGU4T09Bbm9ZQjFlS0hpcm8wNFpPenArektqSjRXMGliRzJMSDFqY2ZMaHhFZEZhTGQyTHQyeXhRcUl2YW42WEI4T1ZqSG1aY3pIWmhsM0dFa1g0dmJPYWtvT212QUVZUjBzbWdBT2V2MUJOV0ZDZXljeWgrZ21rbDlOcmp5MGpTWThuTGVrb1l0cDVsR1J0QkVNZkx0cmdMdlU1N3hDVjlYWDduamtiZDlGUlZ6ZFJYWWQxRlVlMk1qNXNqNEFSY1pqd3R5YUtEZ2VRZDRQUnhhNjZTYVg2K0RXdy9GNVdaVzBDdHVuaVJnVkZIRmVXZ2kxbjFtTG5HNTNLS3Z4YVV2VmVtbnhKNnN3NjV4MjNYREs5ellZalJIVSt6ZWVTQnJZT1JvMmZmemFEY3U1MzlERG9CR0xxWE9DV3ZJYmNpUjB4Qjgvam4yQ01aSCtnMTBCQWM1ZnZOcnZnMGlQSjZpT24xODRhc2hxZ1cyZ0hMYStJMlJ5UllSS3A3Q2J2VjUwVGhXYllZd25UclBCNStBUldsTDVTVGpqcFBMSDZPeGhVeTRVSkhoSGZRVHE5L0o2dUpJSmVOckNqY29iVFpERmxvMEhiM21ZMWFHbHNKYzFDVXlhRmUwWjZvbEFwTXBSUjZsQzl2NG92ZVNNcng3OU9rSnd6aVRCS3Z4dWNCMzdmQnJ5Wk5yT1BvOGQ4SVlzRld3WG5lRTBxYlZlUy9JYUdmVEdWK1p6dThSbmVXdmhmcXlZdzVvdzdBZ3ZqaE1wOXA3STAvUDVhbHlOQ0k1bUh5YU8zY2hjUkdoUk56dVpUWjJsWC8yV0hzZmZtN3drWE4rVG1EcnloNi9GWVdlbGtQMXAvcU9lcW9uV2RhdlhvU3N3bHpnWHVNZktmNksySmhPYkhMbWwyai9zYmRibzR5ajU5dWo1VGhTWW1iU3hpNHhRWDRDTWpLcXlBWExyeEgwTXFmOHJnTU93cHp2M0U4VEQxMzZZWStjUzFDWFFhSUI1NFBYTUNESENrV1VGMmhKUFp5bVFjclY1UlJIeU16K2lXUVRGcDhuZkl6OElhSktMbEQzbHBMdmRLOVEzTlZyaFlzMDNvR1krbG9BNnQ4SkI1TVg5UHhUSTR0cU9acmFKTGU4RDArRmxLOFI2cWE2aXE3WFBMNEhwendYQmtaZUhvZHZZVFhVZUdSODlnbGhoajRWUXZqaDZWVGlGYkZrVm1ORHcwVUQzSjdIUmw5d1VUR3RkTFh2TTNFY3BCZ1dQUVAvWDhGK1FNUTlTaUtHcDJsNHloeGZYYlVOUXVXdTNPTEI1cEt6S2szb3JQK2I4NklHSjlwOXpJeWtSemxWeWs1WTl3VDcrZFdITTRHODlqdkpYVFBPYTM1bVlJUWRLbnlKRzZEUDQ5Z2ZCWG1qNlJWYTZQMkMvaHJtSEpNN2FMbHA3U244UWFOUXZYOVk0K3lCeUVFSm03NDk5U1lDTys5dzZjVnNSNTJlSXRkNjlGVjFjZXlqcEFTb2VvR3ZxWW9mUXdUZkdHQkE9PQ==", "8LQ3qiH5fFoSagDveXbH/7aI6kuqqfaGAJ+jo/FmCsQBpPeTw6EBEj7zZGkt02D37ekW/BOtBZDY05v30VhfWWLAxH1NVQ7xeA5+3hRmoDWSd4aoHTGdjcGdtDAIj6YIDs6tpjp3gi+T/E0ntciJTLBXLyRtd4RiDzGjwSvTCgUJzlPdF80tTe8OOAnoYB1eKHiro04ZOzp+zKjJ4W0ibG2LH1jcfLhxEdFaLd2Lt2yxQqIvan6XB8OVjHmZczHZhl3GEkX4vbOakoOmvAEYR0smgAOev1BNWFCeycyh+gmkl9Nrjy0jSY8nLekoYtp5lGRtBEMfLtrgLvU57xCV9XX7njkbd9FRVzdRXYd1FUe2Mj5sj4ARcZjwtyaKDgeQd4PRxa66SaX6+DWw/F5WZW0CtuniRgVFHFeWgi1n1mLnG53KKvxaUvVemnxJ6sw65x23XDK9zYYjRHU+zeeSBrYORo2ffzaDcu539DDoBGLqXOCWvIbciR0xB8/jn2CMZH+g10BAc5fvNrvg0iPJ6iOn184ashqgW2gHLa+I2RyRYRKp7CbvV50ThWbYYwnTrPB5+ARWlL5STjjpPLH6OxhUy4UJHhHfQTq9/J6uJIJeNrCjcobTZDFlo0Hb3mY1aGlsJc1CUyaFe0Z6olApMpRR6lC9v4oveSMrx79OkJwziTBKvxucB37fBryZNrOPo8d8IYsFWwXneE0qbVeS/IaGfTGV+Zzu8RneWvhfqyYw5ow7AgvjhMp9p7I0/P5alyNCI5mHyaO3chcRGhRNzuZTZ2lX/2WHsffm7wkXN+TmDryh6/FYWelkP1p/qOeqonWdavXoSswlzgXuMfKf6K2JhObHLml2j/sbdbo4yj59uj5ThSYmbSxi4xQX4CMjKqyAXLrxH0Mqf8rgMOwpzv3E8TD136YY+cS1CXQaIB54PXMCDHCkWUF2hJPZymQcrV5RRHyMz+iWQTFp8nfIz8IaJKLlD3lpLvdK9Q3NVrhYs03oGY+loA6t8JB5MX9PxTI4tqOZraJLe8D0+FlK8R6qa6iq7XPL4HpzwXBkZeHodvYTXUeGR89glhhj4VQvjh6VTiFbFkVmNDw0UD3J7HRl9wUTGtdLXvM3EcpBgWPQP/X8F+QMQ9SiKGp2l4yhxfXbUNQuWu3OLB5pKzKk3orP+b86IGJ9p9zIykRzlVyk5Y9wT7+dWHM4G89jvJXTPOa35mYIQdKnyJG6DP49gfBXmj6RVa6P2C/hrmHJM7aLlp7Sn8QaNQvX9Y4+yByEEJm7499SYCO+9w6cVsR52eItd69FV1ceyjpASoeoGvqYofQwTfGGBA==");
}