#!/bin/bash
des_1="openssl des"
des_2="./ft_ssl des"
COL=$(tput cols)
RES_POS=$(($COL - 4))
MOVE_TO_COL="printf \\033[${RES_POS}G"
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
RES=0
DES_MODE=$1
DES_MODE_UP=$(printf "%s" $1 | tr a-z A-Z)
display_res()
{
	if [ $? -eq 0 ]
	then
		$MOVE_TO_COL
		printf "%s" "${GREEN}[OK]${NORMAL}" $'\n'
	else
		$MOVE_TO_COL
		printf "%s" "${RED}[KO]${NORMAL}" $'\n'
		RES=1
	fi
}


test_des_cbc_encode_stdin ()
{
	printf "DES_%s ==>> Encode : %s" "$DES_MODE_UP" "$1"
	diff <(echo $4 | eval "${des_1}-${DES_MODE}" -pass "pass:$2" -S $3 | hexdump -C) <(echo $4 | eval "${des_2}-${DES_MODE}" -p $2 -s $3 | hexdump -C)
	display_res
}

test_des_cbc_encode_file ()
{
	printf "DES_%s ==>> Encode : %s" "$DES_MODE_UP" "$1"
	diff <(eval "${des_1}-${DES_MODE}" -in $4 -pass "pass:$2" -S $3 | hexdump -C) <(eval "${des_2}-${DES_MODE}" -i $4 -p $2 -s $3 | hexdump -C)
	display_res
}

test_des_cbc_decode_stdin ()
{
	printf "DES_%s ==>> Decode : %s" "$DES_MODE_UP" "$1"
	diff <(echo $3 | eval "${des_1}-${DES_MODE}" -pass "pass:$2" | eval "${des_1}-${DES_MODE}" -d -pass "pass:$2" | hexdump -C) <(echo $3 | eval "${des_1}-${DES_MODE}" -pass "pass:$2" | eval "${des_2}-${DES_MODE}" -dp $2 | hexdump -C)
	display_res
}

test_des_cbc_encode_decode_stdin ()
{
	printf "DES_%s ==>> Both : %s" "$DES_MODE_UP" "$1"
	diff <(echo $4 | eval "${des_1}-${DES_MODE}" -pass "pass:$2" | eval "${des_1}-${DES_MODE}" -pass "pass:$2" -d) <(echo $4 | eval "${des_2}-${DES_MODE}" -p $2 | eval "${des_2}-${DES_MODE}" -p $2 -d)
	display_res
}

test_des_cbc_decode_file ()
{
	printf "DES_%s ==>> Decode : %s" "$DES_MODE_UP" "$1"
	diff <(eval "${des_1}-${DES_MODE}" -in $3 -pass "pass:$2" | eval "${des_1}-${DES_MODE}" -d -pass "pass:$2" | hexdump -C) <(eval "${des_1}-${DES_MODE}" -in $3 -pass "pass:$2" | eval "${des_2}-${DES_MODE}" -d -p $2 | hexdump -C)
	display_res
}

test_des_cbc_encode_stdin_base64 ()
{
	printf "DES_%s ==>> Encode (base64) : %s" "$DES_MODE_UP" "$1"
	diff <(echo $4 | eval "${des_1}-${DES_MODE}" -pass "pass:$2" -S $3 -a) <(echo $4 | eval "${des_2}-${DES_MODE}" -p $2 -s $3 -a -b64)
	display_res
}

test_des_cbc_decode_stdin_base64 ()
{
	printf "DES_%s ==>> Decode (base64) : %s" "$DES_MODE_UP" "$1"
	diff <(echo $3 | eval "${des_1}-${DES_MODE}" -pass "pass:$2" -a | eval "${des_1}-${DES_MODE}" -pass "pass:$2" -a -d) <(echo $3 | eval "${des_1}-${DES_MODE}" -pass "pass:$2" -a | eval "${des_2}-${DES_MODE}" -p $2 -ad)
	display_res
}

test_des_cbc_decode_file_base64 ()
{
	printf "DES_%s ==>> Decode (base64) : %s" "$DES_MODE_UP" "$1"
	diff <(eval "${des_1}-${DES_MODE}" -in $3 -pass "pass:$2" -a | eval "${des_1}-${DES_MODE}" -pass "pass:$2" -a -d) <(eval "${des_1}-${DES_MODE}" -in $3 -pass "pass:$2" -a | eval "${des_2}-${DES_MODE}" -p $2 -ad)
	display_res
}

test_des_cbc_encode_file_key ()
{
	printf "DES_%s ==>> Encode (key) : %s" "$DES_MODE_UP" "$1"
	diff <(eval "${des_1}-${DES_MODE}" -in $4 -iv $3 -K $2) <(eval "${des_2}-${DES_MODE}" -i $4 -v $3 -k $2)
	display_res
}

test_des_cbc_encode_file_base64_key ()
{
	printf "DES_%s ==>> Encode (key + base64) : %s" "$DES_MODE_UP" "$1"
	diff <(eval "${des_1}-${DES_MODE}" -in $4 -iv $3 -K $2 -a) <(eval "${des_2}-${DES_MODE}" -i $4 -v $3 -k $2 -a -b64)
	display_res
}

test_des_cbc_decode_file_key ()
{
	printf "DES_%s ==>> Decode (key) : %s" "$DES_MODE_UP" "$1"
	diff <(eval "${des_1}-${DES_MODE}" -in $4 -iv $3 -K $2 | eval "${des_1}-${DES_MODE}" -d -iv $3 -K $2) <(eval "${des_2}-${DES_MODE}" -i $4 -v $3 -k $2 | eval "${des_2}-${DES_MODE}" -dv $3 -k $2)
	display_res
}

test_des_cbc_decode_file_base64_key ()
{
	printf "DES_%s ==>> Decode (key + base64) : %s" "$DES_MODE_UP" "$1"
	diff <(eval "${des_1}-${DES_MODE}" -in $4 -iv $3 -K $2 -a | eval "${des_1}-${DES_MODE}" -d -iv $3 -K $2 -a) <(eval "${des_2}-${DES_MODE}" -i $4 -v $3 -k $2 -a -b64 | eval "${des_2}-${DES_MODE}" -dv $3 -k $2 -a -b64)
	display_res
}

test_des_cbc_encode_stdin "Stdin 0" "toto" "83AACC733B61BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin "Stdin 1" "tutu" "83AACC733B61BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin "Stdin 2" "totu" "83AACC733B61BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin "Stdin 3" "toto" "83BACC733B61EFEB" "salut tu vas bien"
test_des_cbc_encode_stdin "Stdin 4" "toto" "823AACC733B61BFB" "salut tu vas bien"
test_des_cbc_encode_stdin "Stdin 5" "toto" "20AACC733B61BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin "Stdin 6" "toto" "83AACC755561BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin "Stdin 7" "toto" "83AACC733B61BFEB" "saluter"
test_des_cbc_encode_stdin "Stdin 8" "toto" "83AACC733B61BFEB" "salutere"
test_des_cbc_encode_stdin "Stdin 9" "totouuuuutuuuuutuuuuutuuuuutuuutuuuu" "83AACC733B61BFEB" "salutere"
test_des_cbc_encode_stdin "Stdin 10" "wefwefwefwefwefwfeuutuuuu" "83AACC733B61BFEB" "salutere"


test_des_cbc_encode_file "File 0" "toto" "83AACC733B61BFEB" "Makefile"
test_des_cbc_encode_file "File 1" "tutu" "83AACC733B61BFEB" "Makefile"
test_des_cbc_encode_file "File 2" "totu" "83AACC733B61BFEB" "Makefile"
test_des_cbc_encode_file "File 3" "toto" "83BACC733B61EFEB" "Makefile"
test_des_cbc_encode_file "File 4" "toto" "823AACC733B61BFB" "Makefile"
test_des_cbc_encode_file "File 5" "toto" "20AACC733B61BFEB" "Makefile"
test_des_cbc_encode_file "File 6" "toto" "83AACC755561BFEB" "Makefile"
test_des_cbc_encode_file "File 7" "toto" "83AACC733B61BFEB" "author"
test_des_cbc_encode_file "File 8" "toto" "83AACC733B61BFEB" ".gitignore"
test_des_cbc_encode_file "File 9" "totouuuuutuuuuutuuuuutuuuuutuuutuuuu" "83AACC733B61BFEB" "Makefile"
test_des_cbc_encode_file "File 10" "wefwefwefwefwefwfeuutuuuu" "83AACC733B61BFEB" "author"


test_des_cbc_decode_stdin "Stdin 0" "toto" "salut tu vas bien"
test_des_cbc_decode_stdin "Stdin 1" "tutu" "salut tu vas bien"
test_des_cbc_decode_stdin "Stdin 2" "totu" "salut tu vas bien"
test_des_cbc_decode_stdin "Stdin 3" "toto" "saluter"
test_des_cbc_decode_stdin "Stdin 4" "toto" "salutere"
test_des_cbc_decode_stdin "Stdin 5" "totouuuuutuuuuutuuuuutuuuuutuuutuuuu" "salutere"
test_des_cbc_decode_stdin "Stdin 6" "wefwefwefwefwefwfeuutuuuu" "salutere"


test_des_cbc_encode_decode_stdin "Stdin 0" "toto" "salut tu vas bien"
test_des_cbc_encode_decode_stdin "Stdin 1" "tutu" "salut tu vas bien"
test_des_cbc_encode_decode_stdin "Stdin 2" "totu" "salut tu vas bien"
test_des_cbc_encode_decode_stdin "Stdin 3" "toto" "saluter"
test_des_cbc_encode_decode_stdin "Stdin 4" "toto" "salutere"
test_des_cbc_encode_decode_stdin "Stdin 5" "totouuuuutuuuuutuuuuutuuuuutuuutuuuu" "salutere"
test_des_cbc_encode_decode_stdin "Stdin 6" "wefwefwefwefwefwfeuutuuuu" "salutere"

test_des_cbc_decode_file "File 0" "toto" "Makefile"
test_des_cbc_decode_file "File 1" "tutu" "Makefile"
test_des_cbc_decode_file "File 2" "totu" "Makefile"
test_des_cbc_decode_file "File 3" "toto" "author"
test_des_cbc_decode_file "File 4" "toto" ".gitignore"
test_des_cbc_decode_file "File 5" "totouuuuutuuuuutuuuuutuuuuutuuutuuuu" "Makefile"
test_des_cbc_decode_file "File 6" "wefwefwefwefwefwfeuutuuuu" "Makefile"


test_des_cbc_encode_stdin_base64 "Stdin 0" "toto" "83AACC733B61BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin_base64 "Stdin 1" "tutu" "83AACC733B61BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin_base64 "Stdin 2" "totu" "83AACC733B61BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin_base64 "Stdin 3" "toto" "83BACC733B61EFEB" "salut tu vas bien"
test_des_cbc_encode_stdin_base64 "Stdin 4" "toto" "823AACC733B61BFB" "salut tu vas bien"
test_des_cbc_encode_stdin_base64 "Stdin 5" "toto" "20AACC733B61BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin_base64 "Stdin 6" "toto" "83AACC755561BFEB" "salut tu vas bien"
test_des_cbc_encode_stdin_base64 "Stdin 7" "toto" "83AACC733B61BFEB" "saluter"
test_des_cbc_encode_stdin_base64 "Stdin 8" "toto" "83AACC733B61BFEB" "salutere"
test_des_cbc_encode_stdin_base64 "Stdin 9" "totouuuuutuuuuutuuuuutuuuuutuuutuuuu" "83AACC733B61BFEB" "salutere"
test_des_cbc_encode_stdin_base64 "Stdin 10" "wefwefwefwefwefwfeuutuuuu" "83AACC733B61BFEB" "salutere"


test_des_cbc_decode_stdin_base64 "Stdin 0" "toto" "salut tu vas bien"
test_des_cbc_decode_stdin_base64 "Stdin 1" "tutu" "salut tu vas bien"
test_des_cbc_decode_stdin_base64 "Stdin 2" "totu" "salut tu vas bien"
test_des_cbc_decode_stdin_base64 "Stdin 3" "toto" "saluter"
test_des_cbc_decode_stdin_base64 "Stdin 4" "toto" "salutere"
test_des_cbc_decode_stdin_base64 "Stdin 5" "totouuuuutuuuuutuuuuutuuuuutuuutuuuu" "salutere"
test_des_cbc_decode_stdin_base64 "Stdin 6" "wefwefwefwefwefwfeuutuuuu" "salutere"


test_des_cbc_decode_file_base64 "File 0" "toto" "Makefile"
test_des_cbc_decode_file_base64 "File 1" "tutu" "Makefile"
test_des_cbc_decode_file_base64 "File 2" "totu" "Makefile"
test_des_cbc_decode_file_base64 "File 3" "toto" "author"
test_des_cbc_decode_file_base64 "File 4" "toto" ".gitignore"
test_des_cbc_decode_file_base64 "File 5" "totouuuuutuuuuutuuuuutuuuuutuuutuuuu" "Makefile"
test_des_cbc_decode_file_base64 "File 6" "wefwefwefwefwefwfeuutuuuu" "Makefile"


test_des_cbc_encode_file_key "File 0" "BBE3FD3A91C98AFE" "BBE3FD3A91C98AFE" "Makefile"
test_des_cbc_encode_file_key "File 1" "BBE3FD3A91" "BBE3FD3A91C98AFE" "Makefile"
test_des_cbc_encode_file_key "File 2" "BBE3FD5B91C98AFE" "BBE3FD3A91C98AFE" "author"
test_des_cbc_encode_file_key "File 3" "BBE3FD5B91C98AFE" "BBE89" "author"

test_des_cbc_encode_file_base64_key "File 0" "BBE3FD3A91C98AFE" "BBE3FD3A91C98AFE" "Makefile"
test_des_cbc_encode_file_base64_key "File 1" "BBE3FD3A91" "BBE3FD3A91C98AFE" "Makefile"
test_des_cbc_encode_file_base64_key "File 2" "BBE3FD5B91C98AFE" "BBE3FD3A91C98AFE" "author"
test_des_cbc_encode_file_base64_key "File 3" "BBE3FD5B91C98AFE" "BBE89" "author"

test_des_cbc_decode_file_key "File 0" "BBE3FD3A91C98AFE" "BBE3FD3A91C98AFE" "Makefile"
test_des_cbc_decode_file_key "File 1" "BBE3FD3A91" "BBE3FD3A91C98AFE" "Makefile"
test_des_cbc_decode_file_key "File 2" "BBE3FD5B91C98AFE" "BBE3FD3A91C98AFE" "author"
test_des_cbc_decode_file_key "File 3" "BBE3FD5B91C98AFE" "BBE89" "author"


test_des_cbc_decode_file_base64_key "File 0" "BBE3FD3A91C98AFE" "BBE3FD3A91C98AFE" "Makefile"
test_des_cbc_decode_file_base64_key "File 1" "BBE3FD3A91" "BBE3FD3A91C98AFE" "Makefile"
test_des_cbc_decode_file_base64_key "File 2" "BBE3FD5B91C98AFE" "BBE3FD3A91C98AFE" "author"
test_des_cbc_decode_file_base64_key "File 3" "BBE3FD5B91C98AFE" "BBE89" "author"

exit ${RES}
