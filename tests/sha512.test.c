/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512.test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/10 13:00:20 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_sha512_test(char *str, char *res_verif)
{
	t_ft_hash_ctx 	ctx;
	char	*res;

	g_be = is_be();
	ctx.hash = g_hash_table[HT_SHA512];
	ft_sha512_init(&ctx);
	ft_hash_block_update(&ctx, (unsigned char*)str,
		ft_strlen((const char*)str));
	res = ft_hash_block_finish(&ctx);
	cr_assert(!strcmp(res, (const char*)res_verif));
	free(res);
}

Test(SHA512, Simple)
{
	routine_sha512_test("a", "1f40fc92da241694750979ee6cf582f2d5d7d28e18335de05abc54d0560e0f5302860c652bf08d560252aa5e74210546f369fbbbce8c12cfc7957b2652fe9a75");
	routine_sha512_test("ab", "2d408a0717ec188158278a796c689044361dc6fdde28d6f04973b80896e1823975cdbf12eb63f9e0591328ee235d80e9b5bf1aa6a44f4617ff3caf6400eb172d");
}

Test(SHA512, BigUnder512)
{
	routine_sha512_test("hoKgmq3kcZXGR0jNbh+EPI2fIrcKZlDszuVPATUd0Ds=",
		"7f2a86382738485e441e9a94823db7e2b70b62c8242b7822eb2fc0ea092299de1837b662d08acc7c4fb6a557291481107b003e44fa836ae0d235e76000650ff6");
	routine_sha512_test("yuEWkKBs0Uf7tv8FwsnEDC+zomXpbGkjHBB0OvsTufdyrxZp9V04vO",
		"2868cd0acb34d1b7bdd49ea01c6cf4a0098bd93dbff02172105f64c22c82a772efc2407b4dd8997fd10083a0b68c56730939c6173071ff7cd859e30242c58e72");
	routine_sha512_test("7U96W6jnIpgiowEIbK3270sYj/uFoqzpJSmhKyeK+bEz3u+JtiOuK5",
		"eea6a5772a4c34f278103af4b946d91090e3b6cafb78ba41dbe7e934102c15952777321735c3a7dfeea54e848967e03bbc202043c0c982f3df3807dc5a0c64ba");
}

Test(SHA512, BigAbove512)
{
	routine_sha512_test("KLwFM4Rz2Q4MO2BPR0EIsd6IVlW4kqvJQskLSsCrf/grwGzqWwhgiZre67Nv/jEjiVZKWR1EuCDymSblxtRN0F+VqLJKOkrv9xcPE3pKMD3/OYcrjaJQQW4iGT8uZ2KQDN5szkjWHLK7lx+oJbs5CwPdJbe+4gmumpafjhnvsXBstUkju/DSe/35nYgoB2hTpNf1lYAygaABIjP4tLH8XUDj28LeGj+QCGyztqR2Gbg0Xsj1YI06l0A7Zi0jv0VzOKXGF0drc42mxM3oP3s2BzeCJdzGOPyhvQavqodaWTXt+ab5WzuUiBQZC6ss9hIyiAJxtE6Y4A3Iv5XaFa1lxFUxS37YDbKdyhuV6FF2ql+3u85FrntpINb49Tw9DoCmsJQ2ZQRH4BK1nJGSo4Xl2cs0uUxbjiW4PPzrk0UohmOqvuCcJD5L3C4qI4+NM5hgVLh9IjC4467IB1MbxRISOvw4muqV45f5a8Gmf5uvxSYAz2rsrG03I9CbjIdz3q6X64OuJbUfO5LY2nn7cgFCvTp6dNYZ4/FkN3fPgfH+eCZ6EZIcB/iWoHXG9Ex7UgmhF41KjjUUkPz11ceUBni0Wp7ifhHMigYuyWYsJw7uRZcKXHwQk/9JGljtIxqJ++ODrF9W4LqJ1c0h+7Ww2dHMbEvqCLHXjaJnUENjnM1UpnQSMBrU3cUn6bAlkDxNS8egOcSokkZRIjtZyRM8uzLnt+gebKMt5M2qPtPM5J+ga2pTVMEUI/VI+37oE4NcYcAU9s25W+qGaGKwmKWhHJpbdM26vqUj96Ih3fmHVPUAg9YRWCT6m2g2usa5/GvoOp1vKlDWmutUNsh/6bU6i0cWAE/Fa0J+1yoC48roerRJLy8i9URWN9BoqZcWfJg5D1fOw4HRnj2Ysg9Jg4CAeRstlykcYDvBkA+t8hIsSpkB/ddlY94uFmYPQz20NY8VjKYt8cxIMUIrKrWkAFMn1GwBuwFOhk56Yv1bjHeII9hwCGuUq7PH2chKDA2PxVS4T+CkiYl/xaPMGyUpc4Ef7HVZSZ2rzwARjLb/TKbetwdOue4YLTzJc6ZGFe+m4iaeqsaxsW3uI2m6MdhO9OZbStlAMcig4B2GXfoNHaivNvrdiBfPfnXFELWmdErkHT8/sl8B3O+HDZQGH7Mooz97ddAe5BFLJxVvAnIPLPB0xb5J/PVfEz5myikMkHxmoyPKHypP/IOy7Vkn0iRmHTkFEfUZxS5dDLPKCS3F9Bkmn6xQAYilNs0xMp1RZg6T2RPqpTqlJi0JWwX6LmFPJjBi2ioQHQlqswxvOcBJGqV5IhVWzGoEX1sdCmWu0A==",
		"29221077f9dde0c6d87186ec7d1f60c57ec03a2607c9a540cb587eaa9b9e3a614b224e9b87da478ab05e889a0ba6f794ae2b1452bbabbed8fa44eee8f7a9c1c9");
}