/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.test.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/26 11:39:29 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_md5_test(char *str, char *res_verif)
{
	t_ft_hash_ctx 	ctx;
	char	*res;

	g_be = is_be();
	ctx.hash = g_hash_table[HT_MD5];
	ft_md5_init(&ctx);
	ft_hash_block_update(&ctx, (unsigned char*)str, ft_strlen((const char*)str));
	res = ft_hash_block_finish(&ctx);
	cr_assert(!strcmp(res, (const char*)res_verif));
	free(res);
}

Test(MD5, Raw)
{
	t_ft_hash_ctx 	ctx;
	char			*res;

	g_be = is_be();
	ctx.hash = g_hash_table[HT_MD5];
	ft_md5_init(&ctx);
	ctx.opt |= HASH_RAW;
	ft_hash_block_update(&ctx, (unsigned char*)"helloworld", 10);
	res = ft_hash_block_finish(&ctx);
	ft_printf("Res %llx\n", res);
	free(res);
}

Test(MD5, Simple)
{
	routine_md5_test("a", "0cc175b9c0f1b6a831c399e269772661");
	routine_md5_test("ab", "187ef4436122d1cc2f40dc2b92f0eba0");
}

Test(MD5, BigUnder512)
{
	routine_md5_test("hoKgmq3kcZXGR0jNbh+EPI2fIrcKZlDszuVPATUd0Ds=",
		"79574c06d31ffecd3eaca24985c2169e");
	routine_md5_test("yuEWkKBs0Uf7tv8FwsnEDC+zomXpbGkjHBB0OvsTufdyrxZp9V04vO5IBqoa",
		"7b81bc2067b4e7416138c21a323bcd70");
	routine_md5_test("7U96W6jnIpgiowEIbK3270sYj/uFoqzpJSmhKyeK+bEz3u+JtiOuK5PY",
		"e35dbd763dcc2ba0bab5a17dae0de599");
}

Test(MD5, BigAbove512)
{
	routine_md5_test("nOS1AruqjO/YSNsLxsD3hGW8+srn+GLreObQSslK5M+qA7ZHk1Sp23lc"
	"1r697X06"
	"SEGqPqyqRyCythcnhTICTd7GLLVC3wHmJ6l8dG6AGzTAHR0+3vfFWoQufKryutWIf4weccahH"
	"8wUe1BDUB6kxhsksLSAsoGVbwIUKTzA09Z+/+BgFKSHcMRsZCs9Ol4IfN0gnfCEj/d1E8wiXQ"
	"DadMg3R4VJKQt2e9PKiD99K1ak01j7DRXYFHA6SWCLB3hUio3nPxVpZrd2CDDiPCrSM7N+qKX"
	"cBf2vyEDbtQBeuT7wxD4mCuicNLItJ65iarvuwGPMMjBnd96j7fAH4A05LJkI0UHTFdR2jIh0O"
	"wcG1oYaLKQn6Thho+9FMlPQUoWC8qveXcmRWAO+wkoHfxwefPNOPd4CyqlZW63KqWkv0CymxX"
	"wE0EVvrAF/iTDu1vIO6WQwz7jegAiCJyPBsRAb06C9yAUeiVjneOcdOqG0jZvn61Ptijtyj4q"
	"4kqzgJMTjXAP1pj+ODP37oxxEhrlHvPdRiPhsu+GdquXK+wQLVYGX9bJEw2M4XlelqeQ/GQzp"
	"G8r6/8b8kottpPPqsDnuQagdx18Uw/KbFh+Fii1GmcPrhu6GTqucXtauZBQ36xnHBDkm3V39h1"
	"OUwBU51egQ/HrPnaQ=",
		"1c0f49638c0e9396710d76a9d3142f15");
	routine_md5_test("cKtGQWqjSwLX1DrcbGltCrj9e2PoO+Gqkydtg9JZtahiw+MfcJScnStYcBBh33arSsQGKscBo1Hk7svnsUdFAUm9N8E81hswxhlOcuLlGPHwZFwZSZ4jvN3HODLLndHcFg/PQA==",
		"5878f9e0ab2ae6ffb57d0e644499edc3");
}