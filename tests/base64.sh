#!/bin/bash
base64_1="base64"
base64_2="./ft_ssl base64"
COL=$(tput cols)
RES_POS=$(($COL - 4))
MOVE_TO_COL="printf \\033[${RES_POS}G"
# GREEN="\033[92m"
# RED="\033[91m"
# DEFAULT="\033[39m"
# CYAN="\033[36m"
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
RES=0

test_base64_encode ()
{
	printf "BASE64 ==>> Encode : %s" "$1"
	if [ -z "$3" ]
	then
		diff <(eval ${base64_1} $2) <(eval ${base64_2} $2) >/dev/null
	else
		diff <(eval $3 "$4" | eval ${base64_1} $2) <(eval $3 "$4" | eval ${base64_2} $2) >/dev/null
	fi
	if [ $? -eq 0 ]
	then
		$MOVE_TO_COL
		printf "%s" "${GREEN}[OK]${NORMAL}" $'\n'
	else
		$MOVE_TO_COL
		printf "%s" "${RED}[KO]${NORMAL}" $'\n'
		RES=1
	fi
}

test_base64_decode ()
{
	printf "BASE64 ==>> Decode : %s" "$1"
	diff <(eval ${base64_1} $2 | ${base64_1} $3) <(eval ${base64_1} $2 | ${base64_2} $4)
	if [ $? -eq 0 ]
	then
		$MOVE_TO_COL
		printf "%s" "${GREEN}[OK]${NORMAL}" $'\n'
	else
		$MOVE_TO_COL
		printf "%s" "${RED}[KO]${NORMAL}" $'\n'
		RES=1
	fi
}

test_base64_encode "Stdin 0" "" "echo" "Salut Salut"
test_base64_encode "Stdin 1" "" "echo" "Comment va mon gros"
test_base64_encode "Stdin 2" "" "echo" "Cest le feu copain"
test_base64_encode "Stdin 3" "-i-" "echo" "Ouais de OUF"

test_base64_encode "File 0" "-i Makefile"
test_base64_encode "File 1" "-i author"
test_base64_encode "File 2" "-i .gitignore"

test_base64_encode "File 0 (binary)" "-i ft_ssl"
test_base64_encode "File 1 (binary)" "-i ft_ssl_test"

test_base64_encode "Multiple inputs 0" "-i ft_ssl_test -i ft_ssl -i Makefile"
test_base64_encode "Multiple inputs 1" "-i ft_ssl_test -i ft_ssl"

test_base64_encode "Break 0" "-b64 -i ft_ssl_test"
test_base64_encode "Break 1" "-b4 -i ft_ssl_test"
test_base64_encode "Break 2" "-b1 -i ft_ssl_test"
test_base64_encode "Break 3" "-b0 -i ft_ssl_test"

test_base64_encode "Multiple Break 0" "-b0 -b1 -i ft_ssl_test"
test_base64_encode "Multiple Break 1" "-b0 -b52 -b64 -i ft_ssl_test"

test_base64_encode "Param 0" "Makefile"
test_base64_encode "Param 1" "Makefile author"

test_base64_decode "File 0" "-i Makefile" "-D" "-d"
test_base64_decode "File 1" "-i ./ft_ssl_test" "-D" "-d"

test_base64_decode "Break 0" "-b50 -i Makefile" "-D" "-d"
test_base64_decode "Break 1" "-b1 -i ./ft_ssl_test" "-D" "-d"

test_base64_decode "Multiple input 0" "-i Makefile -i author" "-D" "-d"
test_base64_decode "Multiple input 1" "-i ./ft_ssl_test -i Makefile -i .gitignore" "-D" "-d"

test_base64_decode "Multiple mode 0" "-i Makefile" "-D" "-deeeeeeeeeeeeeeeeeeed"
test_base64_decode "Multiple mode 1" "-i ./ft_ssl_test" "-D" "-dedededededededed"

exit ${RES}