/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512224.test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/10 13:00:20 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_sha512224_test(char *str, char *res_verif)
{
	t_ft_hash_ctx 	ctx;
	char	*res;

	g_be = is_be();
	ctx.hash = g_hash_table[HT_SHA512224];
	ft_sha512224_init(&ctx);
	ft_hash_block_update(&ctx, (unsigned char*)str,
		ft_strlen((const char*)str));
	res = ft_hash_block_finish(&ctx);
	cr_assert(!strcmp(res, (const char*)res_verif));
	free(res);
}

Test(SHA512224, Simple)
{
	routine_sha512224_test("a", "d5cdb9ccc769a5121d4175f2bfdd13d6310e0d3d361ea75d82108327");
	routine_sha512224_test("ab", "b35878d07bfedf39fc638af08547eb5d1072d8546319f247b442fbf5");
}

Test(SHA512224, BigUnder512)
{
	routine_sha512224_test("hoKgmq3kcZXGR0jNbh+EPI2fIrcKZlDszuVPATUd0Ds=",
		"77ba20885bd05f32af195bbe36dd28e5308bfd6c92d045f563bd27d9");
	routine_sha512224_test("yuEWkKBs0Uf7tv8FwsnEDC+zomXpbGkjHBB0OvsTufdyrxZp9V04vO",
		"89102170a24a7999e9802395cacd855d6cf119a9bf74a6fe56f3e9f3");
	routine_sha512224_test("7U96W6jnIpgiowEIbK3270sYj/uFoqzpJSmhKyeK+bEz3u+JtiOuK5",
		"62ea0a5bacbb14cb946e02c90acbcc9dd54599efa0621a3427a83546");
}

Test(SHA512224, BigAbove512)
{
	routine_sha512224_test("KLwFM4Rz2Q4MO2BPR0EIsd6IVlW4kqvJQskLSsCrf/grwGzqWwhgiZre67Nv/jEjiVZKWR1EuCDymSblxtRN0F+VqLJKOkrv9xcPE3pKMD3/OYcrjaJQQW4iGT8uZ2KQDN5szkjWHLK7lx+oJbs5CwPdJbe+4gmumpafjhnvsXBstUkju/DSe/35nYgoB2hTpNf1lYAygaABIjP4tLH8XUDj28LeGj+QCGyztqR2Gbg0Xsj1YI06l0A7Zi0jv0VzOKXGF0drc42mxM3oP3s2BzeCJdzGOPyhvQavqodaWTXt+ab5WzuUiBQZC6ss9hIyiAJxtE6Y4A3Iv5XaFa1lxFUxS37YDbKdyhuV6FF2ql+3u85FrntpINb49Tw9DoCmsJQ2ZQRH4BK1nJGSo4Xl2cs0uUxbjiW4PPzrk0UohmOqvuCcJD5L3C4qI4+NM5hgVLh9IjC4467IB1MbxRISOvw4muqV45f5a8Gmf5uvxSYAz2rsrG03I9CbjIdz3q6X64OuJbUfO5LY2nn7cgFCvTp6dNYZ4/FkN3fPgfH+eCZ6EZIcB/iWoHXG9Ex7UgmhF41KjjUUkPz11ceUBni0Wp7ifhHMigYuyWYsJw7uRZcKXHwQk/9JGljtIxqJ++ODrF9W4LqJ1c0h+7Ww2dHMbEvqCLHXjaJnUENjnM1UpnQSMBrU3cUn6bAlkDxNS8egOcSokkZRIjtZyRM8uzLnt+gebKMt5M2qPtPM5J+ga2pTVMEUI/VI+37oE4NcYcAU9s25W+qGaGKwmKWhHJpbdM26vqUj96Ih3fmHVPUAg9YRWCT6m2g2usa5/GvoOp1vKlDWmutUNsh/6bU6i0cWAE/Fa0J+1yoC48roerRJLy8i9URWN9BoqZcWfJg5D1fOw4HRnj2Ysg9Jg4CAeRstlykcYDvBkA+t8hIsSpkB/ddlY94uFmYPQz20NY8VjKYt8cxIMUIrKrWkAFMn1GwBuwFOhk56Yv1bjHeII9hwCGuUq7PH2chKDA2PxVS4T+CkiYl/xaPMGyUpc4Ef7HVZSZ2rzwARjLb/TKbetwdOue4YLTzJc6ZGFe+m4iaeqsaxsW3uI2m6MdhO9OZbStlAMcig4B2GXfoNHaivNvrdiBfPfnXFELWmdErkHT8/sl8B3O+HDZQGH7Mooz97ddAe5BFLJxVvAnIPLPB0xb5J/PVfEz5myikMkHxmoyPKHypP/IOy7Vkn0iRmHTkFEfUZxS5dDLPKCS3F9Bkmn6xQAYilNs0xMp1RZg6T2RPqpTqlJi0JWwX6LmFPJjBi2ioQHQlqswxvOcBJGqV5IhVWzGoEX1sdCmWu0A==",
		"6dc3a04de6847cd3d76a0ac7bddf3b7cd20fc30ce2d5c0cac6348b7a");
}