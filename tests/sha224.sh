#!/bin/bash
SHA224_1="shasum -a 224 | cut -d' ' -f 1 "
SHA224_2="./ft_ssl sha224"
COL=$(tput cols)
RES_POS=$(($COL - 4))
MOVE_TO_COL="printf \\033[${RES_POS}G"
# GREEN="\033[92m"
# RED="\033[91m"
# DEFAULT="\033[39m"
# CYAN="\033[36m"
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)
RES=0

test_sha224 ()
{
	printf "SHA224 ==>> %s" "$1"
	if [ -z "$3" ]
	then
		diff <(eval ${SHA224_1} $2) <(eval ${SHA224_2} $2)
	else
		diff <(eval $3 "$4" | eval ${SHA224_1} $2) <(eval $3 "$4" | eval ${SHA224_2} $2)
	fi
	if [ $? -eq 0 ]
	then
		$MOVE_TO_COL
		printf "%s" "${GREEN}[OK]${NORMAL}" $'\n'
	else
		$MOVE_TO_COL
		printf "%s" "${RED}[KO]${NORMAL}" $'\n'
		RES=1
	fi
}

test_sha224 "Stdin 0" "" "echo" "Salut Salut"
test_sha224 "Stdin 1" "" "echo" "Comment va mon gros"
test_sha224 "Stdin 2" "" "echo" "Cest le feu copain"
test_sha224 "Stdin 3" "" "echo" "Ouais de OUF"

test_sha224 "File 0" "" "cat" "Makefile"
test_sha224 "File 1" "" "cat" ".gitignore"
test_sha224 "File 2" "" "cat" "author"

exit ${RES}