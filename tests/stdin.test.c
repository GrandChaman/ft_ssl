/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stdin.test.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/17 11:24:25 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>

char	cb_update(void *param, char *buf, size_t size)
{
	cr_assert(param == (void*)1);
	cr_assert(buf != NULL);
	cr_assert(size != 0);
	return (0);
}

Test(stdin, Normal)
{
	FILE *stdin;

	stdin = cr_get_redirected_stdin();

	fprintf(stdin, "HelloWorld");
	fflush(stdin);
	fclose(stdin);
	cr_assert(ft_read_fd(0, (void*)1, &cb_update, 0) == 0);
}

Test(stdin, Echo)
{
	FILE *stdin;
	FILE *stdout;

	stdin = cr_get_redirected_stdin();
	stdout = cr_get_redirected_stdout();

	fprintf(stdin, "HelloWorld\n");
	fflush(stdin);
	fclose(stdin);
	cr_assert(ft_read_fd(0, (void*)1, &cb_update, FT_READ_ECHO) == 0);
	cr_file_match_str(stdout, "HelloWorld\n");
	fclose(stdout);
}