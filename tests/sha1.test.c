/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha1.test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 10:09:38 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/10 13:00:20 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <stdlib.h>
#include <criterion/criterion.h>

char		g_be;


void	routine_sha1_test(char *str, char *res_verif)
{
	t_ft_hash_ctx 	ctx;
	char	*res;

	g_be = is_be();
	ctx.hash = g_hash_table[HT_SHA1];
	ft_sha1_init(&ctx);
	ft_hash_block_update(&ctx, (unsigned char*)str,
		ft_strlen((const char*)str));
	res = ft_hash_block_finish(&ctx);
	cr_assert(!strcmp(res, (const char*)res_verif));
	free(res);
}

Test(SHA1, Simple)
{
	routine_sha1_test("a", "86f7e437faa5a7fce15d1ddcb9eaeaea377667b8");
	routine_sha1_test("ab", "da23614e02469a0d7c7bd1bdab5c9c474b1904dc");
}

Test(SHA1, BigUnder512)
{
	routine_sha1_test("hoKgmq3kcZXGR0jNbh+EPI2fIrcKZlDszuVPATUd0Ds=",
		"17adc27bb132b2c2fe2abed711328ebf9b38fcc9");
	routine_sha1_test("yuEWkKBs0Uf7tv8FwsnEDC+zomXpbGkjHBB0OvsTufdyrxZp9V04vO5IBqoa",
		"6282b26fce9e68b99c182018d2a46c106a138ec3");
	routine_sha1_test("7U96W6jnIpgiowEIbK3270sYj/uFoqzpJSmhKyeK+bEz3u+JtiOuK5PY",
		"e1be9afb04827c0a450e55b86b978f0847ca7a2b");
}

Test(SHA1, BigAbove512)
{
	routine_sha1_test("ryYu8x/ep7CaAMSWpYMEn+wGyM3PRI283YqxC2zgjxDec/Bk/ykwlNdr"
	"VCSlxi5cxeQHovKdgFpTeJ92EdoQ5DwiMOiNU4aryFCa2rYytzrCWLajhKCX/8pPtFKs4Gf4La"
	"AL0cUnlBjprCpOt00Qg3HSa9WniIxMPhRfv+4GujpWoJHL6AWIp14Ux2J/DfSVYG+IXTh5zwrE"
	"g1YRU+CbSjpZJ8PyMeoCVdNwgtMC7vtwnZJc53jFGnDhRWe76x6h2Ejpf3+XZUlRuRBsySmsNa"
	"yXiLitaBvvXfZ9VWTm7TYFD2Bs+QZgTUBu/alH7p+dL9qxJdZxshDGUB52f54sQAJOTlPnKiI3"
	"9mbTtI5wQBfRWJ5mFsc/ajnG5HYOfMVyElu8l0xzC0D1w0j0tt4A1xy3ZLThMc/Y1XRJ+I1E7z"
	"X2lPnEf+xu70lfcTN8MGPyuJfNYteEey/VpBi62QkSmInbikTVjunVg4gpRNu3/lhunzWN0fAw"
	"l5c/x6SNenGxai0BcVbvoUTdN9qyzArkby72PTEovdr8vZ3lk19X+CG0dMawj3F38cjQEVE7h/"
	"u45ar930AWRKYLeDPYfpKptgB2GLnA83oFyV9CTwRRn9rIrzS5MPudOqhjmYCHDZGpM58JRFUt"
	"aQdsJOGqjsHDSdM3A8hzGceKJqxrs//tDGtoUUl8LF0lmR7cn6TDDm3Mw+cZ2HXpaZKbaUhyOm"
	"U1Te3JXhvjsv2vwGKrFh8FWpDISZOEWc25KvyAgS7Mjq7XWDz9h75Lwb+/aolzvR4PQ47fQrS3"
	"pm4j",
		"ec8e070239994f7c7c89045b914d5df45379c762");
}