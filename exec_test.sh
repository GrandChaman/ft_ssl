#!/bin/bash

make test

check_ret()
{
	eval $1 $2
	if [ $? -ne 0 ]
	then
		exit 1
	fi
}

check_ret "./ft_ssl_test"
check_ret "./tests/md5.sh"
check_ret "./tests/sha1.sh"
check_ret "./tests/sha224.sh"
check_ret "./tests/sha256.sh"
check_ret "./tests/sha384.sh"
check_ret "./tests/sha512.sh"
check_ret "./tests/sha512_224.sh"
check_ret "./tests/sha512_256.sh"
check_ret "./tests/des.sh" "ecb"
check_ret "./tests/des.sh" "cbc"
check_ret "./tests/des.sh" "ofb"
check_ret "./tests/des.sh" "cfb"
check_ret "./tests/base64.sh"
