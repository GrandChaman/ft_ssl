/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_fd.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/10 14:31:26 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/01 13:37:26 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

char		ft_read_fd(int fd, void *param,
	char (*update)(void*, char*, size_t), uint8_t opt)
{
	char	buf[READ_BLOCK_AL + 1];
	ssize_t	nb_read;
	ssize_t	nb_read_tot;
	char	ret;

	ft_bzero(buf, READ_BLOCK_AL + 1);
	nb_read = 0;
	nb_read_tot = 0;
	while ((nb_read = read(fd, buf + nb_read_tot % READ_BLOCK_AL,
		READ_BLOCK_AL - nb_read_tot % READ_BLOCK_AL)) > 0)
	{
		nb_read_tot += nb_read;
		if (opt & FT_READ_ECHO)
			ft_printf("%s", buf);
		if ((ret = update(param, buf, (size_t)nb_read_tot)))
			return (ret);
		ft_bzero(buf, nb_read);
		if (nb_read_tot >= READ_BLOCK_AL && opt & FT_READ_SINGLE_ROUND)
			break ;
		nb_read_tot = 0;
	}
	if (nb_read_tot && !nb_read)
		return (update(param, buf, (size_t)nb_read_tot));
	return (nb_read < 0 ? 1 : 0);
}

int			ft_ssl_open_input_stream(t_copt_parsed *opts, size_t pos)
{
	int		fd;
	char	*file;

	if (!opts)
		return (-1);
	file = "-";
	fd = 0;
	if (opts->opts[pos])
		file = copt_get_last_opt(opts, pos).argv[0];
	if (ft_strcmp(file, "-"))
		fd = open(file, O_RDONLY);
	if (!opts->opts[pos] && opts->params &&
		(file = opts->params->argv[0]))
		fd = open(file, O_RDONLY);
	if (fd < 0)
		ft_ssl_error(opts, file);
	return (fd);
}

int			ft_ssl_open_output_stream(t_copt_parsed *opts, size_t pos)
{
	int		fd;
	char	*file;

	if (!opts)
		return (-1);
	file = "-";
	fd = 1;
	if (opts->opts[pos])
		file = copt_get_last_opt(opts, pos).argv[0];
	if (ft_strcmp(file, "-"))
		fd = open(file, O_WRONLY | O_CREAT | O_TRUNC,
			S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
	if (fd < 0)
		ft_ssl_error(opts, file);
	return (fd);
}
