/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_to_string.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 15:27:58 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/30 11:43:55 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

char		*ft_hash_to_string(t_ft_hash_ctx *ctx, uint8_t rev, uint8_t reg_nb)
{
	size_t	i;
	char	*res;

	i = -1;
	res = ft_strnew(reg_nb * 8 * sizeof(char));
	if (ctx->opt & HASH_RAW)
		while (++i < reg_nb)
		{
			if (!rev)
				ft_memrcpy(res + i * 4, ctx->registers[i].c, 4);
			else
				ft_memcpy(res + i * 4, ctx->registers[i].c, 4);
		}
	else
		while (++i < reg_nb)
			ft_snprintf(res + i * 8, 8, "%08x", rev ?
				brev_32(ctx->registers[i].x32) : ctx->registers[i].x32);
	ft_hash_destroy(ctx);
	return (res);
}

char		*ft_hash_to_string_long(t_ft_hash_ctx *ctx,
	uint8_t rev, uint8_t reg_nb)
{
	size_t	i;
	char	*res;

	i = -1;
	res = ft_strnew(reg_nb * 16 * sizeof(char));
	while (++i < reg_nb)
		ft_snprintf(res + i * 16, 16, "%016llx", rev
			? brev_64(ctx->registers[i].x64) : ctx->registers[i].x64);
	ft_hash_destroy(ctx);
	return (res);
}
