/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha256.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 17:40:09 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:36:58 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

void	ft_sha256_init(t_ft_hash_ctx *ctx)
{
	ft_hash_init(ctx);
	ctx->registers[REG_A].x32 = 0x6A09E667;
	ctx->registers[REG_B].x32 = 0xBB67AE85;
	ctx->registers[REG_C].x32 = 0x3C6EF372;
	ctx->registers[REG_D].x32 = 0xA54FF53A;
	ctx->registers[REG_E].x32 = 0x510E527F;
	ctx->registers[REG_F].x32 = 0x9B05688C;
	ctx->registers[REG_G].x32 = 0x1F83D9AB;
	ctx->registers[REG_H].x32 = 0x5BE0CD19;
}

char	*ft_sha256_finish(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	hb.words[14] = (!g_be ? brev_32(ctx->total_size.x64 >> 32)
		: ctx->total_size.x64 >> 32);
	hb.words[15] = (!g_be ? brev_32((ctx->total_size.x64 << 32) >> 32)
		: (ctx->total_size.x64 << 32) >> 32);
	ctx->hash.update(ctx, hb);
	return (ft_hash_to_string(ctx, 0, ctx->hash.reg_nb));
}
