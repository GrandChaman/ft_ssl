/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 17:40:09 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:06:55 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

void	ft_sha512_init(t_ft_hash_ctx *ctx)
{
	ft_hash_init(ctx);
	ctx->registers[REG_A].x64 = 0x6A09E667F3BCC908;
	ctx->registers[REG_B].x64 = 0xBB67AE8584CAA73B;
	ctx->registers[REG_C].x64 = 0x3C6EF372FE94F82B;
	ctx->registers[REG_D].x64 = 0xA54FF53A5F1D36F1;
	ctx->registers[REG_E].x64 = 0x510E527FADE682D1;
	ctx->registers[REG_F].x64 = 0x9B05688C2B3E6C1F;
	ctx->registers[REG_G].x64 = 0x1F83D9ABFB41BD6B;
	ctx->registers[REG_H].x64 = 0x5BE0CD19137E2179;
}

char	*ft_sha512_finish(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	hb.bwords[14] = !g_be ? brev_64(ctx->total_size.x128 >> 64)
		: ctx->total_size.x128 >> 64;
	hb.bwords[15] = !g_be ? brev_64((ctx->total_size.x128 << 64) >> 64)
		: (ctx->total_size.x128 << 64) >> 64;
	ctx->hash.update(ctx, hb);
	return (ft_hash_to_string_long(ctx, 0, ctx->hash.reg_nb));
}
