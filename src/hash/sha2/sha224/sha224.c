/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 17:40:09 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 14:43:58 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

void	ft_sha224_init(t_ft_hash_ctx *ctx)
{
	ft_hash_init(ctx);
	ctx->registers[REG_A].x32 = 0xC1059ED8;
	ctx->registers[REG_B].x32 = 0x367CD507;
	ctx->registers[REG_C].x32 = 0x3070DD17;
	ctx->registers[REG_D].x32 = 0xF70E5939;
	ctx->registers[REG_E].x32 = 0xFFC00B31;
	ctx->registers[REG_F].x32 = 0x68581511;
	ctx->registers[REG_G].x32 = 0x64F98FA7;
	ctx->registers[REG_H].x32 = 0xBEFA4FA4;
}

char	*ft_sha224_finish(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	hb.words[14] = (!g_be ? brev_32(ctx->total_size.x64 >> 32)
		: ctx->total_size.x64 >> 32);
	hb.words[15] = (!g_be ? brev_32((ctx->total_size.x64 << 32) >> 32)
		: (ctx->total_size.x64 << 32) >> 32);
	ctx->hash.update(ctx, hb);
	return (ft_hash_to_string(ctx, 0, ctx->hash.reg_nb - 1));
}
