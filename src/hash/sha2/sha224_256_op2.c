/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224_256_op2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 15:12:51 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:04:59 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

uint32_t		sha2_op_bsig0(uint32_t x)
{
	return (right_rot(x, 2) ^ right_rot(x, 13) ^ right_rot(x, 22));
}

uint32_t		sha2_op_bsig1(uint32_t x)
{
	return (right_rot(x, 6) ^ right_rot(x, 11) ^ right_rot(x, 25));
}

uint32_t		sha2_op_ssig0(uint32_t x)
{
	return (right_rot(x, 7) ^ right_rot(x, 18) ^ (x >> 3));
}

uint32_t		sha2_op_ssig1(uint32_t x)
{
	return (right_rot(x, 17) ^ right_rot(x, 19) ^ (x >> 10));
}
