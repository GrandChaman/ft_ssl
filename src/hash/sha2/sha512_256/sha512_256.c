/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_256.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 17:40:09 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:07:41 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

void	ft_sha512256_init(t_ft_hash_ctx *ctx)
{
	ft_hash_init(ctx);
	ctx->registers[REG_A].x64 = 0x22312194FC2BF72C;
	ctx->registers[REG_B].x64 = 0x9F555FA3C84C64C2;
	ctx->registers[REG_C].x64 = 0x2393B86B6F53B151;
	ctx->registers[REG_D].x64 = 0x963877195940EABD;
	ctx->registers[REG_E].x64 = 0x96283EE2A88EFFE3;
	ctx->registers[REG_F].x64 = 0xBE5E1E2553863992;
	ctx->registers[REG_G].x64 = 0x2B0199FC2C85B8AA;
	ctx->registers[REG_H].x64 = 0x0EB72DDC81C52CA2;
}

char	*ft_sha512256_finish(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	char		*str;

	hb.bwords[14] = !g_be ? brev_64(ctx->total_size.x128 >> 64)
		: ctx->total_size.x128 >> 64;
	hb.bwords[15] = !g_be ? brev_64((ctx->total_size.x128 << 64) >> 64)
		: (ctx->total_size.x128 << 64) >> 64;
	ctx->hash.update(ctx, hb);
	str = ft_hash_to_string_long(ctx, 0, ctx->hash.reg_nb);
	str[64] = 0;
	return (str);
}
