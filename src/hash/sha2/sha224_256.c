/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha224_256.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 15:12:11 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:04:38 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

void	ft_sha2_routine(uint32_t reg[SHA2_REG_NB], uint32_t tmp[2])
{
	reg[REG_H] = reg[REG_G];
	reg[REG_G] = reg[REG_F];
	reg[REG_F] = reg[REG_E];
	reg[REG_E] = reg[REG_D] + tmp[0];
	reg[REG_D] = reg[REG_C];
	reg[REG_C] = reg[REG_B];
	reg[REG_B] = reg[REG_A];
	reg[REG_A] = tmp[0] + tmp[1];
}

void	ft_sha2_update(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	size_t		i;
	uint32_t	w[64];
	uint32_t	reg[SHA2_REG_NB];
	uint32_t	tmp[2];

	i = -1;
	while (++i < 16)
		w[i] = (!g_be ? brev_32(hb.words[i]) : hb.words[i]);
	i = 15;
	ft_hash_copy_reg(ctx, reg);
	while (++i < 64)
		w[i] = sha2_op_ssig1(w[i - 2]) + w[i - 7]
			+ sha2_op_ssig0(w[i - 15]) + w[i - 16];
	i = -1;
	while (++i < 64)
	{
		tmp[0] = reg[REG_H] + sha2_op_bsig1(reg[REG_E])
			+ sha2_op_ch(reg[REG_E], reg[REG_F], reg[REG_G])
			+ g_sha2_k[i] + w[i];
		tmp[1] = sha2_op_bsig0(reg[REG_A])
			+ sha2_op_maj(reg[REG_A], reg[REG_B], reg[REG_C]);
		ft_sha2_routine(reg, tmp);
	}
	add_reg(ctx, (uint32_t*)reg);
}
