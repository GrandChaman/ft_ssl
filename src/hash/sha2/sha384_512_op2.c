/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha384_512_op2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/13 15:12:51 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:05:58 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

uint64_t		sha3_op_bsig0(uint64_t x)
{
	return (right_rot_long(x, 28) ^ right_rot_long(x, 34)
		^ right_rot_long(x, 39));
}

uint64_t		sha3_op_bsig1(uint64_t x)
{
	return (right_rot_long(x, 14) ^ right_rot_long(x, 18)
		^ right_rot_long(x, 41));
}

uint64_t		sha3_op_ssig0(uint64_t x)
{
	return (right_rot_long(x, 1) ^ right_rot_long(x, 8) ^ (x >> 7));
}

uint64_t		sha3_op_ssig1(uint64_t x)
{
	return (right_rot_long(x, 19) ^ right_rot_long(x, 61) ^ (x >> 6));
}
