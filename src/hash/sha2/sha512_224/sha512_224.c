/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha512_224.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 17:40:09 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:07:14 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

void	ft_sha512224_init(t_ft_hash_ctx *ctx)
{
	ft_hash_init(ctx);
	ctx->registers[REG_A].x64 = 0x8C3D37C819544DA2;
	ctx->registers[REG_B].x64 = 0x73E1996689DCD4D6;
	ctx->registers[REG_C].x64 = 0x1DFAB7AE32FF9C82;
	ctx->registers[REG_D].x64 = 0x679DD514582F9FCF;
	ctx->registers[REG_E].x64 = 0x0F6D2B697BD44DA8;
	ctx->registers[REG_F].x64 = 0x77E36F7304C48942;
	ctx->registers[REG_G].x64 = 0x3F9D85A86A1D36C8;
	ctx->registers[REG_H].x64 = 0x1112E6AD91D692A1;
}

char	*ft_sha512224_finish(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	char		*str;

	hb.bwords[14] = !g_be ? brev_64(ctx->total_size.x128 >> 64)
		: ctx->total_size.x128 >> 64;
	hb.bwords[15] = !g_be ? brev_64((ctx->total_size.x128 << 64) >> 64)
		: (ctx->total_size.x128 << 64) >> 64;
	ctx->hash.update(ctx, hb);
	str = ft_hash_to_string_long(ctx, 0, ctx->hash.reg_nb);
	str[56] = 0;
	return (str);
}
