/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 15:27:58 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/29 11:11:25 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void		zero_hash_block(t_hash_block *res)
{
	size_t i;

	i = 0;
	while (i < 64)
		res->blocks[i++] = 0;
}

uint32_t	brev_32(uint32_t b)
{
	return (((b >> 24) & 0xff) | ((b << 8) & 0xff0000) | ((b >> 8) & 0xff00)
		| ((b << 24) & 0xff000000));
}

uint32_t	left_rot(uint32_t x, unsigned char c)
{
	return (((x) << (c)) | ((x) >> (32 - (c))));
}

uint32_t	right_rot(uint32_t x, unsigned char c)
{
	return ((x >> c) | (x << (32 - c)));
}

void		add_reg(t_ft_hash_ctx *ctx, uint32_t *reg2)
{
	size_t i;

	i = 0;
	while (i < ctx->hash.reg_nb)
	{
		ctx->registers[i].x32 += reg2[i];
		i++;
	}
}
