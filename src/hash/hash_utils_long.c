/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_utils_long.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 15:27:58 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/29 11:11:25 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

uint64_t	brev_64(uint64_t b)
{
	return ((b & 0x00000000000000FFUL) << 56 | (b & 0x000000000000FF00UL) << 40
		| (b & 0x0000000000FF0000UL) << 24 | (b & 0x00000000FF000000UL) << 8
		| (b & 0x000000FF00000000UL) >> 8 | (b & 0x0000FF0000000000UL) >> 24
		| (b & 0x00FF000000000000UL) >> 40 | (b & 0xFF00000000000000UL) >> 56);
}

uint64_t	left_rot_long(uint64_t x, unsigned char c)
{
	return (((x) << (c)) | ((x) >> (64 - (c))));
}

uint64_t	right_rot_long(uint64_t x, unsigned char c)
{
	return ((x >> c) | (x << (64 - c)));
}

void		add_reg_long(t_ft_hash_ctx *ctx, uint64_t *reg2)
{
	size_t i;

	i = 0;
	while (i < ctx->hash.reg_nb)
	{
		ctx->registers[i].x64 += reg2[i];
		i++;
	}
}
