/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_opts.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 16:35:43 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/27 15:53:55 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"

void		ft_hash_print(t_copt_parsed *opts, t_ft_hash_ctx *ctx,
	char *str, char is_file)
{
	char	*hash;

	hash = ft_hash_block_finish(ctx);
	if (opts->opts[HT_OPT_Q] || !str)
		ft_printf("%s\n", hash);
	else if (opts->opts[HT_OPT_R])
		ft_printf("%s %c%s%c\n", hash, is_file ? '\0' : '"', str,
			is_file ? '\0' : '"');
	else
		ft_printf("%s (%c%s%c) = %s\n", ctx->hash.title,
			is_file ? '\0' : '"', str, is_file ? '\0' : '"', hash);
	free(hash);
}

char		ft_hash_files(t_ft_hash_ctx *ctx, t_copt_parsed *opts)
{
	size_t			i;
	int				fd;
	char			ret;

	i = -1;
	ret = 0;
	while (++i < opts->params->argc)
	{
		if ((fd = open(opts->params->argv[i], O_RDONLY)) < 0)
		{
			ret = ft_ssl_error(opts, opts->params->argv[i]);
			continue ;
		}
		ctx->hash.init(ctx);
		if (ft_read_fd(fd, (void*)ctx,
			(char(*)(void*, char*, size_t))ft_hash_block_update, 0))
		{
			ret = ft_ssl_error(opts, opts->params->argv[i]);
			ft_hash_destroy(ctx);
		}
		else
			ft_hash_print(opts, ctx, opts->params->argv[i], 1);
		close(fd);
	}
	return (ret);
}

char		ft_hash_strings(t_ft_hash_ctx *ctx, t_copt_parsed *res,
	t_copt_llist *llist)
{
	size_t			len;

	ctx->hash.init(ctx);
	len = ft_strlen(llist->opt->argv[llist->index]);
	if (len)
		ft_hash_block_update(ctx,
			(unsigned char*)llist->opt->argv[llist->index], len);
	ft_hash_print(res, ctx, llist->opt->argv[llist->index], 0);
	return (0);
}
