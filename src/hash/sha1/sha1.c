/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha1.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 17:40:09 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:31:59 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

void	ft_sha1_init(t_ft_hash_ctx *ctx)
{
	ft_hash_init(ctx);
	ctx->registers[REG_A].x32 = 0x67452301;
	ctx->registers[REG_B].x32 = 0xEFCDAB89;
	ctx->registers[REG_C].x32 = 0x98BADCFE;
	ctx->registers[REG_D].x32 = 0x10325476;
	ctx->registers[REG_E].x32 = 0xC3D2E1F0;
}

void	ft_sha1_update(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	size_t		i;
	uint32_t	w[80];
	uint32_t	reg[SHA1_REG_NB];
	uint32_t	tmp;

	i = -1;
	while (++i < 16)
		w[i] = (!g_be ? brev_32(hb.words[i]) : hb.words[i]);
	i = 15;
	ft_hash_copy_reg(ctx, reg);
	while (++i < 80)
		w[i] = left_rot((w[i - 3] ^ w[i - 8] ^ w[i - 14] ^ w[i - 16]), 1);
	i = -1;
	while (++i < 80)
	{
		tmp = left_rot(reg[REG_A], 5) + sha1_op(i, reg[REG_B],
			reg[REG_C], reg[REG_D]) + reg[REG_E] + w[i]
				+ sha1_const(i);
		reg[REG_E] = reg[REG_D];
		reg[REG_D] = reg[REG_C];
		reg[REG_C] = left_rot(reg[REG_B], 30);
		reg[REG_B] = reg[REG_A];
		reg[REG_A] = tmp;
	}
	add_reg(ctx, (uint32_t*)reg);
}

char	*ft_sha1_finish(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	hb.words[14] = (!g_be ? brev_32(ctx->total_size.x64 >> 32)
		: ctx->total_size.x64 >> 32);
	hb.words[15] = (!g_be ? brev_32((ctx->total_size.x64 << 32) >> 32)
		: (ctx->total_size.x64 << 32) >> 32);
	ft_sha1_update(ctx, hb);
	return (ft_hash_to_string(ctx, 0, ctx->hash.reg_nb));
}
