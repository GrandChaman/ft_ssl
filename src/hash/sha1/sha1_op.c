/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sha1_op.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/08 17:46:17 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:03:28 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

uint32_t	sha1_op_f(uint32_t x, uint32_t y, uint32_t z)
{
	return ((x & y) | (~x & z));
}

uint32_t	sha1_op_g(uint32_t x, uint32_t y, uint32_t z)
{
	return (x ^ y ^ z);
}

uint32_t	sha1_op_h(uint32_t x, uint32_t y, uint32_t z)
{
	return ((x & y) | (x & z) | (y & z));
}

uint32_t	sha1_op(uint32_t i, uint32_t x, uint32_t y, uint32_t z)
{
	if (i <= 19)
		return (sha1_op_f(x, y, z));
	if (i >= 20 && i <= 39)
		return (sha1_op_g(x, y, z));
	if (i >= 40 && i <= 59)
		return (sha1_op_h(x, y, z));
	return (sha1_op_g(x, y, z));
}

uint32_t	sha1_const(uint32_t i)
{
	if (i <= 19)
		return (0x5A827999);
	if (i >= 20 && i <= 39)
		return (0x6ED9EBA1);
	if (i >= 40 && i <= 59)
		return (0x8F1BBCDC);
	return (0xCA62C1D6);
}
