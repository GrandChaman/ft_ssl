/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   md5.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/11 14:14:15 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 11:31:39 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "libft.h"

const t_ft_md5_round	g_md5_rounds[64] = {
	{&md5_op_f, 0, 0xd76aa478, 7},
	{&md5_op_f, 1, 0xe8c7b756, 12},
	{&md5_op_f, 2, 0x242070db, 17},
	{&md5_op_f, 3, 0xc1bdceee, 22},
	{&md5_op_f, 4, 0xf57c0faf, 7},
	{&md5_op_f, 5, 0x4787c62a, 12},
	{&md5_op_f, 6, 0xa8304613, 17},
	{&md5_op_f, 7, 0xfd469501, 22},
	{&md5_op_f, 8, 0x698098d8, 7},
	{&md5_op_f, 9, 0x8b44f7af, 12},
	{&md5_op_f, 10, 0xffff5bb1, 17},
	{&md5_op_f, 11, 0x895cd7be, 22},
	{&md5_op_f, 12, 0x6b901122, 7},
	{&md5_op_f, 13, 0xfd987193, 12},
	{&md5_op_f, 14, 0xa679438e, 17},
	{&md5_op_f, 15, 0x49b40821, 22},
	{&md5_op_g, 1, 0xf61e2562, 5},
	{&md5_op_g, 6, 0xc040b340, 9},
	{&md5_op_g, 11, 0x265e5a51, 14},
	{&md5_op_g, 0, 0xe9b6c7aa, 20},
	{&md5_op_g, 5, 0xd62f105d, 5},
	{&md5_op_g, 10, 0x02441453, 9},
	{&md5_op_g, 15, 0xd8a1e681, 14},
	{&md5_op_g, 4, 0xe7d3fbc8, 20},
	{&md5_op_g, 9, 0x21e1cde6, 5},
	{&md5_op_g, 14, 0xc33707d6, 9},
	{&md5_op_g, 3, 0xf4d50d87, 14},
	{&md5_op_g, 8, 0x455a14ed, 20},
	{&md5_op_g, 13, 0xa9e3e905, 5},
	{&md5_op_g, 2, 0xfcefa3f8, 9},
	{&md5_op_g, 7, 0x676f02d9, 14},
	{&md5_op_g, 12, 0x8d2a4c8a, 20},
	{&md5_op_h, 5, 0xfffa3942, 4},
	{&md5_op_h, 8, 0x8771f681, 11},
	{&md5_op_h, 11, 0x6d9d6122, 16},
	{&md5_op_h, 14, 0xfde5380c, 23},
	{&md5_op_h, 1, 0xa4beea44, 4},
	{&md5_op_h, 4, 0x4bdecfa9, 11},
	{&md5_op_h, 7, 0xf6bb4b60, 16},
	{&md5_op_h, 10, 0xbebfbc70, 23},
	{&md5_op_h, 13, 0x289b7ec6, 4},
	{&md5_op_h, 0, 0xeaa127fa, 11},
	{&md5_op_h, 3, 0xd4ef3085, 16},
	{&md5_op_h, 6, 0x04881d05, 23},
	{&md5_op_h, 9, 0xd9d4d039, 4},
	{&md5_op_h, 12, 0xe6db99e5, 11},
	{&md5_op_h, 15, 0x1fa27cf8, 16},
	{&md5_op_h, 2, 0xc4ac5665, 23},
	{&md5_op_i, 0, 0xf4292244, 6},
	{&md5_op_i, 7, 0x432aff97, 10},
	{&md5_op_i, 14, 0xab9423a7, 15},
	{&md5_op_i, 5, 0xfc93a039, 21},
	{&md5_op_i, 12, 0x655b59c3, 6},
	{&md5_op_i, 3, 0x8f0ccc92, 10},
	{&md5_op_i, 10, 0xffeff47d, 15},
	{&md5_op_i, 1, 0x85845dd1, 21},
	{&md5_op_i, 8, 0x6fa87e4f, 6},
	{&md5_op_i, 15, 0xfe2ce6e0, 10},
	{&md5_op_i, 6, 0xa3014314, 15},
	{&md5_op_i, 13, 0x4e0811a1, 21},
	{&md5_op_i, 4, 0xf7537e82, 6},
	{&md5_op_i, 11, 0xbd3af235, 10},
	{&md5_op_i, 2, 0x2ad7d2bb, 15},
	{&md5_op_i, 9, 0xeb86d391, 21},
};

void	ft_md5_init(t_ft_hash_ctx *ctx)
{
	ft_hash_init(ctx);
	ctx->registers[REG_A].x32 = 0x67452301;
	ctx->registers[REG_B].x32 = 0xefcdab89;
	ctx->registers[REG_C].x32 = 0x98badcfe;
	ctx->registers[REG_D].x32 = 0x10325476;
}

void	ft_md5_update(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	t_ft_md5_round	round;
	size_t			i;
	uint32_t		reg[MD5_REG_NB];
	uint32_t		tmp;
	uint32_t		f;

	i = -1;
	while (++i < MD5_REG_NB)
		reg[i] = (g_be
			? brev_32(ctx->registers[i].x32) : ctx->registers[i].x32);
	i = -1;
	while (++i < 64)
	{
		round = g_md5_rounds[i];
		f = round.f(reg[REG_B], reg[REG_C], reg[REG_D]);
		tmp = reg[REG_D];
		reg[REG_D] = reg[REG_C];
		reg[REG_C] = reg[REG_B];
		reg[REG_B] = reg[REG_B]
			+ left_rot(reg[REG_A] + f + hb.words[round.k] + round.sin,
			round.rot);
		reg[REG_A] = tmp;
	}
	add_reg(ctx, (uint32_t*)reg);
}

char	*ft_md5_finish(t_ft_hash_ctx *ctx, t_hash_block hb)
{
	hb.words[14] = (g_be ? brev_32(ctx->total_size.x64 & 0xffffffff)
		: ctx->total_size.x64 & 0xffffffff);
	hb.words[15] = (g_be ? brev_32((ctx->total_size.x64 >> 32) & 0xffffffff)
		: ((ctx->total_size.x64 >> 32) & 0xffffffff));
	ft_md5_update(ctx, hb);
	return (ft_hash_to_string(ctx, 1, ctx->hash.reg_nb));
}
