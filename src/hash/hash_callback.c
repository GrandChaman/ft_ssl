/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash_callback.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 16:27:31 by fle-roy           #+#    #+#             */
/*   Updated: 2019/05/22 10:56:02 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "copt.h"
#include "ft_ssl.h"
#include "libft.h"

char	ft_hash_select(t_copt_parsed *opts)
{
	size_t	i;

	i = -1;
	while (g_hash_table[++i].init)
		if (!ft_stricmp(g_hash_table[i].title, opts->cmd->text))
			return (ft_hash_start(g_hash_table[i], opts));
	return (-1);
}
