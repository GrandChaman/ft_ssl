/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hash.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/29 16:35:43 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/26 11:33:55 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_hash.h"

t_ft_hash		g_hash_table[] = {
	{"MD5", MD5_REG_NB, 64, 8, 32, &ft_md5_init, &ft_md5_update,
		&ft_md5_finish},
	{"SHA1", SHA1_REG_NB, 64, 8, 40, &ft_sha1_init, &ft_sha1_update,
		&ft_sha1_finish},
	{"SHA224", SHA2_REG_NB, 64, 8, 56, &ft_sha224_init, &ft_sha2_update,
		&ft_sha224_finish},
	{"SHA256", SHA2_REG_NB, 64, 8, 64, &ft_sha256_init, &ft_sha2_update,
		&ft_sha256_finish},
	{"SHA384", SHA3_REG_NB, 128, 16, 96, &ft_sha384_init, &ft_sha3_update,
		&ft_sha384_finish},
	{"SHA512", SHA3_REG_NB, 128, 16, 128, &ft_sha512_init, &ft_sha3_update,
		&ft_sha512_finish},
	{"SHA512224", SHA3_REG_NB, 128, 16, 56, &ft_sha512224_init, &ft_sha3_update,
		&ft_sha512224_finish},
	{"SHA512256", SHA3_REG_NB, 128, 16, 64, &ft_sha512256_init, &ft_sha3_update,
		&ft_sha512256_finish},
	{NULL, 0, 0, 0, 0, NULL, NULL, NULL}
};

char		*ft_hash_exec(size_t hash, char *str, size_t len)
{
	t_ft_hash_ctx	ctx;

	ctx.hash = g_hash_table[hash];
	ctx.hash.init(&ctx);
	ft_hash_block_update(&ctx, (unsigned char*)str,
		len ? len : ft_strlen((const char*)str));
	return (ft_hash_block_finish(&ctx));
}

void		ft_hash_destroy(t_ft_hash_ctx *ctx)
{
	size_t	i;

	i = 0;
	while (i < ctx->hash.reg_nb)
		ctx->registers[i++].x64 = 0;
	ft_memdel((void**)&ctx->registers);
	ft_bzero(ctx->buf, ctx->hash.block_size);
	ft_memdel((void**)&ctx->buf);
	ctx->cursor = 0;
	ctx->total_size.x128 = 0;
}

void		ft_hash_init(t_ft_hash_ctx *ctx)
{
	ctx->registers = ft_memalloc(sizeof(t_ft_num) * ctx->hash.reg_nb);
	ctx->buf = ft_memalloc(sizeof(char) * ctx->hash.block_size);
	ft_bzero(ctx->buf, ctx->hash.block_size);
	ctx->cursor = 0;
	ctx->total_size.x128 = 0;
	ctx->opt = 0;
}

char		ft_hash_routine(t_ft_hash_ctx *ctx, t_copt_parsed *opts,
	t_copt_llist *llist)
{
	char			ret;

	ret = 0;
	if (!llist || llist->opt->id == HT_OPT_P)
	{
		ctx->hash.init(ctx);
		ret = ft_read_fd(0, (void*)ctx,
			(char(*)(void*, char*, size_t))ft_hash_block_update,
				opts->opts[HT_OPT_P] ? FT_READ_ECHO : 0);
		if (ret)
		{
			ft_ssl_error(opts, "stdin");
			ft_hash_destroy(ctx);
		}
		else
			ft_hash_print(opts, ctx, NULL, 0);
		close(0);
	}
	else if (llist->opt->id == HT_OPT_S)
		ret = ft_hash_strings(ctx, opts, llist);
	return (ret);
}

char		ft_hash_start(t_ft_hash hash, t_copt_parsed *opts)
{
	t_ft_hash_ctx	ctx;
	t_copt_llist	*llist;
	char			ret;

	ret = 0;
	ctx.hash = hash;
	llist = opts->list;
	if (!opts->opts[HT_OPT_S] && !opts->params && !opts->opts[HT_OPT_P])
		ft_hash_routine(&ctx, opts, NULL);
	while (llist)
	{
		if (ft_hash_routine(&ctx, opts, llist))
			ret = 1;
		llist = llist->next;
	}
	if (opts->params)
		ret = ft_hash_files(&ctx, opts);
	return (ret);
}
