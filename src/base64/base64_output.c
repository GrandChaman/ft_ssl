/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64_output.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 15:38:48 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/30 11:49:25 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_base64.h"

char	ft_base64_write_fd(t_ft_base64 *base64)
{
	ssize_t ret;

	ret = write(base64->out_fd, base64->obuf.buf, base64->obuf.cursor);
	dbuf_clear(&base64->obuf);
	return (ret < 0 ? 1 : 0);
}

char	ft_base64_encode_write_fd(t_ft_base64 *base64)
{
	int		ret;
	size_t	width;
	char	*fchar;
	size_t	i;

	if (!base64->width)
		return (ft_base64_write_fd(base64));
	ret = 0;
	i = 0;
	fchar = "\n";
	while (!ret && i < base64->obuf.cursor)
	{
		width = (base64->cwrote % base64->width ?
			base64->width - base64->cwrote % base64->width : base64->width);
		if (i + width > base64->obuf.cursor)
		{
			width = base64->obuf.cursor - i;
			fchar = "";
		}
		ret = ft_printf("%.*s%s", width, base64->obuf.buf + i, fchar) < 0;
		base64->cwrote += width;
		i += width;
	}
	dbuf_clear(&base64->obuf);
	return (ret ? 1 : 0);
}
