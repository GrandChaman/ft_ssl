/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:58:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/03 10:43:26 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_base64.h"

char	ft_base64_encode_cb(t_copt_parsed *opts, t_ft_base64 *base64)
{
	if (ft_read_fd(base64->in_fd, (void*)base64,
		(char(*)(void*, char*, size_t))&ft_base64_encode_update, 0))
	{
		return (ft_ssl_error(opts, (opts->opts[B64_OPT_I]
			? copt_get_last_opt(opts, B64_OPT_I).argv[0] : "stdin")));
	}
	ft_base64_encode_finish(base64);
	if (!ft_base64_encode_write_fd(base64))
		ft_putchar_fd('\n', base64->out_fd);
	else
	{
		return (ft_ssl_error(opts, (opts->opts[B64_OPT_O]
			? copt_get_last_opt(opts, B64_OPT_O).argv[0] : "stdout")));
	}
	return (0);
}

char	ft_base64_decode_cb(t_copt_parsed *opts, t_ft_base64 *base64)
{
	if (ft_read_fd(base64->in_fd, (void*)base64,
		(char(*)(void*, char*, size_t))&ft_base64_decode_update, 0))
	{
		return (ft_ssl_error(opts, (opts->opts[B64_OPT_I]
			? copt_get_last_opt(opts, B64_OPT_I).argv[0] : "stdin")));
	}
	ft_base64_decode_finish(base64);
	if (ft_base64_write_fd(base64))
	{
		return (ft_ssl_error(opts, (opts->opts[B64_OPT_O]
			? copt_get_last_opt(opts, B64_OPT_O).argv[0] : "stdout")));
	}
	return (0);
}

char	ft_base64_cb(t_copt_parsed *opts)
{
	char		mode;
	char		ret;
	t_ft_base64	base64;

	ft_bzero((void**)&base64, sizeof(base64));
	mode = copt_get_mode(opts, B64_OPT_E, B64_OPT_D, B64_OPT_E);
	if (ft_base64_init(opts, &base64))
		ret = 1;
	else if (mode == B64_OPT_E)
		ret = ft_base64_encode_cb(opts, &base64);
	else
		ret = ft_base64_decode_cb(opts, &base64);
	ft_base64_destroy(&base64);
	return (ret);
}
