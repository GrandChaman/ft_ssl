/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/30 15:38:48 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/30 12:01:38 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_base64.h"

const char		g_b64_encode_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijkl"\
"mnopqrstuvwxyz0123456789+/";
const uint8_t	g_b64_decode_table[256] = {
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	62,
	'\0',
	'\0',
	'\0',
	63,
	52,
	53,
	54,
	55,
	56,
	57,
	58,
	59,
	60,
	61,
	62,
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	0,
	1,
	2,
	3,
	4,
	5,
	6,
	7,
	8,
	9,
	10,
	11,
	12,
	13,
	14,
	15,
	16,
	17,
	18,
	19,
	20,
	21,
	22,
	23,
	24,
	25,
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	26,
	27,
	28,
	29,
	30,
	31,
	32,
	33,
	34,
	35,
	36,
	37,
	38,
	39,
	40,
	41,
	42,
	43,
	44,
	45,
	46,
	47,
	48,
	49,
	50,
	51,
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
	'\0',
};

void			ft_base64_remove_whitespace(char *buf, size_t *len)
{
	size_t	cursor;
	size_t	offset;
	uint8_t	mode;
	uint8_t	is_ws;

	cursor = 0;
	offset = 0;
	mode = 0;
	while (cursor < *len)
		if ((is_ws = ft_iswhitespace(buf[cursor + offset])) && !mode)
		{
			mode = 1;
			offset = 1;
		}
		else if (mode && is_ws)
			offset++;
		else if (mode && !is_ws)
		{
			ft_memmove(buf + cursor, buf + cursor + offset,
				((*len -= offset) + 1) - cursor);
			mode = 0;
			offset = 0;
		}
		else if (!mode && !is_ws)
			cursor++;
}

char			ft_base64_init(t_copt_parsed *opts, t_ft_base64 *base64)
{
	if (!opts)
	{
		base64->in_fd = -1;
		base64->out_fd = -1;
	}
	else if ((base64->in_fd = ft_ssl_open_input_stream(opts, B64_OPT_I)) < 0)
		return (1);
	else if ((base64->out_fd = ft_ssl_open_output_stream(opts, B64_OPT_O))
		< 0)
		return (1);
	base64->dangling_len = 0;
	ft_bzero(base64->dangling, 3);
	dbuf_init(&base64->obuf);
	base64->cwrote = 0;
	base64->width = (opts && opts->opts[B64_OPT_B]
		? ft_atoi(copt_get_last_opt(opts, B64_OPT_B).argv[0]) : 0);
	base64->copt = opts;
	return (0);
}

void			ft_base64_destroy(t_ft_base64 *base64)
{
	if (base64->in_fd >= 0)
		close(base64->in_fd);
	if (base64->out_fd >= 0)
		close(base64->out_fd);
	dbuf_destroy(&base64->obuf);
}
