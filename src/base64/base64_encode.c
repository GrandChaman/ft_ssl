/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64_encode.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:58:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/30 11:53:55 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_base64.h"

void		base64_encode(uint8_t *dest, uint8_t *src, size_t *i)
{
	dest[0] = g_b64_encode_table[(src[0] >> 2)];
	dest[1] = g_b64_encode_table[(((src[0] << 4) & 0x3F) | src[1] >> 4)];
	dest[2] = g_b64_encode_table[(((src[1] << 2) & 0x3F) | src[2] >> 6)];
	dest[3] = g_b64_encode_table[(src[2] & 0x3F)];
	if (i)
		*i += 3;
}

char		ft_base64_encode_update(t_ft_base64 *b64, char *str, size_t len)
{
	size_t	i;
	uint8_t	buf[READ_BLOCK_AL + 3];
	uint8_t	tmp[5];
	size_t	nlen;
	size_t	ret;

	i = 0;
	ret = 0;
	ft_bzero(tmp, 5);
	ft_memcpy(buf, b64->dangling, b64->dangling_len);
	nlen = (len > READ_BLOCK_AL ? READ_BLOCK_AL : len);
	ft_memcpy(buf + b64->dangling_len, str, nlen);
	nlen += b64->dangling_len;
	b64->dangling_len = nlen % 3;
	while (i < nlen - b64->dangling_len)
	{
		base64_encode(tmp, buf + i, &i);
		dbuf_append(&b64->obuf, (char*)tmp);
	}
	ft_bzero(tmp, 4);
	ft_memcpy(b64->dangling, buf + nlen - b64->dangling_len, b64->dangling_len);
	ret += b64->out_fd >= 0 ? ft_base64_encode_write_fd(b64) : 0;
	ret += len > READ_BLOCK_AL ? ft_base64_encode_update(b64,
		str + READ_BLOCK_AL, len - READ_BLOCK_AL) : 0;
	return (ret ? 1 : 0);
}

char		*ft_base64_encode_finish(t_ft_base64 *base64)
{
	uint8_t	tmp_in[4];
	uint8_t	tmp_out[5];

	ft_bzero(tmp_in, 4);
	ft_bzero(tmp_out, 5);
	if (base64->dangling_len)
	{
		ft_memcpy(tmp_in, base64->dangling, base64->dangling_len);
		base64_encode(tmp_out, tmp_in, NULL);
		ft_memset(tmp_out + base64->dangling_len + 1, '=',
			3 - base64->dangling_len);
		dbuf_append(&base64->obuf, (char*)tmp_out);
		ft_bzero(base64->dangling, 2);
		base64->dangling_len = 0;
	}
	if (base64->out_fd >= 0)
		return (NULL);
	return (base64->obuf.buf);
}
