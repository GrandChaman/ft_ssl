/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   base64_wrapper.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/03 11:07:54 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/14 16:04:49 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_base64.h"

char		*ft_base64_encode(char *str, size_t len)
{
	t_ft_base64	base64;
	size_t		i;
	size_t		count;

	ft_base64_init(NULL, &base64);
	if (!len)
		len = ft_strlen(str);
	i = 0;
	while (i < len)
	{
		count = (len - i > READ_BLOCK_AL ? READ_BLOCK_AL : len - i);
		ft_base64_encode_update(&base64, str + i, count);
		i += count;
	}
	ft_base64_encode_finish(&base64);
	return (base64.obuf.buf);
}

char		*ft_base64_decode(char *str, size_t len)
{
	t_ft_base64	base64;
	size_t		i;
	size_t		count;

	ft_base64_init(NULL, &base64);
	if (!len)
		len = ft_strlen(str);
	i = 0;
	while (i < len)
	{
		count = (len - i > READ_BLOCK_AL ? READ_BLOCK_AL : len - i);
		ft_base64_decode_update(&base64, str + i, count);
		i += count;
	}
	ft_base64_decode_finish(&base64);
	return (base64.obuf.buf);
}
