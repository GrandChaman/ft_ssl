/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 14:04:14 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/01 13:24:15 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "copt.h"
#include "libft.h"

char			g_be;

int				main(int argc, const char **argv)
{
	t_copt_parsed		res;
	char				ret;

	g_be = is_be();
	ret = copt_load(g_copt, &res, argc, argv);
	copt_free_parsed(&res);
	return (ret);
}
