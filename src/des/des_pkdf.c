/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_pkdf.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/04 16:02:32 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/27 15:54:16 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"
#include "ft_ssl_hash.h"
#include <pwd.h>

void	ft_des_pkdf(t_ft_des *des, char *pass, char *salt)
{
	char			tmp[_PASSWORD_LEN + 8];
	size_t			pass_len;
	char			*hash;
	char			*half[2];

	pass_len = ft_strlen(pass);
	ft_memcpy(tmp, pass, pass_len);
	if (salt)
		ft_memcpy(tmp + pass_len, salt, 8);
	else
		ft_memrcpy(tmp + pass_len, &des->salt.x64, 8);
	ft_bzero(tmp + pass_len + 8, _PASSWORD_LEN - pass_len);
	hash = ft_hash_exec(HT_MD5, tmp, pass_len + 8);
	half[0] = ft_strsub(hash, 0, 16);
	half[1] = ft_strsub(hash, 16, 16);
	des->key.x64 = ft_atoi_hex(half[0]);
	des->iv.x64 = ft_atoi_hex(half[1]);
	ft_strdel(&hash);
	ft_strdel(&half[0]);
	ft_strdel(&half[1]);
}

char	*ft_des_get_salt(void)
{
	int		fd;
	ssize_t	readb;
	char	*salt;

	readb = 0;
	if ((fd = open(RANDOM_DEVICE, O_RDONLY)) < 0)
		return (NULL);
	salt = ft_strnew(8);
	while (readb < 8)
		if ((readb = read(fd, salt + readb, 8 - readb)) < 0)
		{
			ft_strdel(&salt);
			close(fd);
			return (NULL);
		}
	close(fd);
	return (salt);
}
