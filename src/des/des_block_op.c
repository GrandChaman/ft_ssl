/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_block_op.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/07 09:44:52 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/30 19:47:15 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"

const uint8_t		g_des_ip[] = {
	58, 50, 42, 34, 26, 18, 10, 2,
	60, 52, 44, 36, 28, 20, 12, 4,
	62, 54, 46, 38, 30, 22, 14, 6,
	64, 56, 48, 40, 32, 24, 16, 8,
	57, 49, 41, 33, 25, 17, 9, 1,
	59, 51, 43, 35, 27, 19, 11, 3,
	61, 53, 45, 37, 29, 21, 13, 5,
	63, 55, 47, 39, 31, 23, 15, 7
};

const uint8_t		g_des_fp[] = {
	40, 8, 48, 16, 56, 24, 64, 32,
	39, 7, 47, 15, 55, 23, 63, 31,
	38, 6, 46, 14, 54, 22, 62, 30,
	37, 5, 45, 13, 53, 21, 61, 29,
	36, 4, 44, 12, 52, 20, 60, 28,
	35, 3, 43, 11, 51, 19, 59, 27,
	34, 2, 42, 10, 50, 18, 58, 26,
	33, 1, 41, 9, 49, 17, 57, 25
};

void				ft_des_ip(t_ft_num *block)
{
	t_ft_num	res;
	uint8_t		i;

	res.x64 = 0;
	i = -1;
	while (++i < 64)
		res.x64 |= (unsigned long)(((block->x64 & ((unsigned long)1
			<< ((64 - g_des_ip[i])))) ? 1 : 0)) << (64 - i - 1);
	block->x64 = res.x64;
}

void				ft_des_fp(t_ft_num *block)
{
	t_ft_num	res;
	uint8_t		i;

	res.x64 = 0;
	i = -1;
	while (++i < 64)
		res.x64 |= (unsigned long)(((block->x64 & ((unsigned long)1
			<< ((64 - g_des_fp[i])))) ? 1 : 0)) << (64 - i - 1);
	block->x64 = res.x64;
}

void				ft_des_process_block(t_ft_num *block, t_ft_num k[16],
	uint8_t enc)
{
	t_ft_couple_num	c;
	uint32_t		lsave;
	uint8_t			i;
	uint8_t			kshift;
	uint8_t			kstart;

	i = -1;
	kshift = (enc == DES_OPT_E ? 1 : -1);
	kstart = (enc == DES_OPT_E ? 0 : 15);
	ft_des_ip(block);
	ft_des_split_64(*block, &c);
	while (++i < 16)
	{
		lsave = c.left.x32;
		c.left.x32 = c.right.x32;
		ft_des_f(&c.right, k[kstart]);
		kstart += kshift;
		c.right.x32 ^= lsave;
	}
	block->x64 = (((uint64_t)c.right.x32 << 32) | c.left.x32);
	ft_des_fp(block);
}
