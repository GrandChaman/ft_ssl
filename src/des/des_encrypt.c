/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_encrypt.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:58:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/02 11:02:58 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"

char	ft_des_print_salt(t_ft_des *des)
{
	char	buf[17];
	ssize_t ret;

	ret = ft_snprintf(buf, 16, "%s%c%c%c%c%c%c%c%c", SALTED_STRING,
		des->salt.c[7], des->salt.c[6], des->salt.c[5], des->salt.c[4],
		des->salt.c[3], des->salt.c[2], des->salt.c[1], des->salt.c[0]);
	dbuf_append_w_len(des->obuf, buf, 16);
	return (ret < 0 ? 1 : 0);
}

char	ft_des_finish_enc(t_ft_des *des)
{
	char	buf[8];
	uint8_t nb;

	if (!(des->cipher_opt & DES_NO_PAD))
	{
		nb = 8 - des->ibuf->cursor % 8;
		ft_memset(buf, nb, nb);
		dbuf_append_w_len(des->ibuf, buf, nb);
	}
	return (ft_des_routine(des, 1));
}
