/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_opts_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/04 16:02:32 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/30 12:34:31 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_ssl.h"

char		ft_des_check_hex_long(size_t nb, char **args)
{
	size_t	i;
	size_t	ii;

	ii = 0;
	while (ii < nb)
	{
		i = 0;
		while (args[ii][i])
			if ((args[ii][i] >= 'a' && args[ii][i] <= 'f')
				|| (args[ii][i] >= 'A' && args[ii][i] <= 'F')
				|| (args[ii][i] >= '0' && args[ii][i] <= '9'))
				i++;
			else
				return (1);
		if (i != 16)
			return (1);
		ii++;
	}
	return (0);
}

char		ft_des_check_hex(size_t nb, char **args)
{
	size_t	i;
	size_t	ii;

	ii = 0;
	while (ii < nb)
	{
		i = 0;
		while (args[ii][i])
			if ((args[ii][i] >= 'a' && args[ii][i] <= 'f')
				|| (args[ii][i] >= 'A' && args[ii][i] <= 'F')
				|| (args[ii][i] >= '0' && args[ii][i] <= '9'))
				i++;
			else
				return (1);
		ii++;
	}
	return (0);
}
