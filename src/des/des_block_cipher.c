/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_block_cipher.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:58:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/06/30 12:48:33 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"

void	ft_des_routine_cbc(t_ft_des *des, char *data, char *buf)
{
	t_ft_num	block;
	t_ft_num	save;

	ft_memrcpy(&block.x64, data, 8);
	save.x64 = block.x64;
	if (des->mode == DES_OPT_E)
		block.x64 ^= des->iv.x64;
	ft_des_process_block(&block, des->skey, des->mode);
	if (des->mode == DES_OPT_D)
		block.x64 ^= des->iv.x64;
	ft_memrcpy(buf, &block.x64, 8);
	des->iv.x64 = (des->mode == DES_OPT_E ? block.x64 : save.x64);
	des->last.x64 = block.x64;
}

void	ft_des_routine_cfb(t_ft_des *des, char *data, char *buf)
{
	t_ft_num	block;
	t_ft_num	save;

	ft_memrcpy(&block.x64, data, 8);
	save.x64 = block.x64;
	ft_des_process_block(&des->iv, des->skey, DES_OPT_E);
	block.x64 ^= des->iv.x64;
	des->iv.x64 = (des->mode == DES_OPT_E ? block.x64 : save.x64);
	ft_memrcpy(buf, &block.x64, 8);
}

void	ft_des_routine_ecb(t_ft_des *des, char *data, char *buf)
{
	t_ft_num	block;

	ft_memrcpy(&block.x64, data, 8);
	ft_des_process_block(&block, des->skey, des->mode);
	ft_memrcpy(buf, &block.x64, 8);
	des->last.x64 = block.x64;
}

void	ft_des_routine_ofb(t_ft_des *des, char *data, char *buf)
{
	t_ft_num	block;

	ft_memrcpy(&block.x64, data, 8);
	ft_des_process_block(&des->iv, des->skey, DES_OPT_E);
	block.x64 ^= des->iv.x64;
	ft_memrcpy(buf, &block.x64, 8);
}
