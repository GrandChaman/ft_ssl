/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/04 16:02:32 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/01 09:16:56 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"

void		ft_des_split_64(t_ft_num input, t_ft_couple_num *res)
{
	res->left.x32 = input.x64 >> 32;
	res->right.x32 = input.x64 & 0xFFFFFFFF;
}

void		ft_des_split_56(t_ft_num input, t_ft_couple_num *res)
{
	res->left.x28 = input.x56 >> 28;
	res->right.x28 = input.x56 & 0xFFFFFFF;
}

void		ft_des_split_48(t_ft_num input, t_ft_couple_num *res)
{
	res->left.x48 = input.x48 >> 24;
	res->right.x24 = input.x48 & 0xFFFFFF;
}

void		ft_des_destroy(t_ft_des *des)
{
	if (des->fd_in >= 0)
		close(des->fd_in);
	if (des->fd_out >= 0)
		close(des->fd_out);
	ft_base64_destroy(&des->base64);
	dbuf_destroy(des->ibuf);
	ft_memdel((void**)&des->ibuf);
	dbuf_destroy(des->obuf);
	ft_memdel((void**)&des->obuf);
}
