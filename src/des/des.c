/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:58:27 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/02 14:06:04 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"
#include <pwd.h>

t_ft_des_block_cipher_mode	g_des_block_cipher_mode[] =
{
	{"des", &ft_des_routine_cbc, DES_IV},
	{"des-ecb", &ft_des_routine_ecb, 0},
	{"des-cbc", &ft_des_routine_cbc, DES_IV},
	{"des-ofb", &ft_des_routine_ofb, DES_IV | DES_NO_PAD | DES_SYM},
	{"des-cfb", &ft_des_routine_cfb, DES_IV | DES_NO_PAD | DES_SYM},
	{NULL, NULL, 0}
};

char						ft_des_opt_init_fd(t_copt_parsed *opts,
	t_ft_des *des)
{
	if ((des->fd_in = ft_ssl_open_input_stream(opts, DES_OPT_I)) < 0)
		return (des->fd_in);
	if ((des->fd_out = ft_ssl_open_output_stream(opts, DES_OPT_O)) < 0)
		return (des->fd_out);
	if (opts->opts[DES_OPT_A])
	{
		des->base64.out_fd = (des->mode == DES_OPT_E ? des->fd_out : -1);
		des->base64.in_fd = (des->mode == DES_OPT_D ? des->fd_in : -1);
		des->base64.width = (opts->opts[DES_OPT_B]
			? ft_atoi(copt_get_last_opt(opts, DES_OPT_B).argv[0]) : 0);
	}
	return (0);
}

char						ft_des_crypto_init(t_copt_parsed *opts,
	t_ft_des *des, char *pass, char *salt)
{
	char ret;

	ret = -1;
	if (salt)
		ft_memrcpy(des->salt.c, salt, 8);
	if (des->mode == DES_OPT_D)
	{
		if ((ret = ft_read_fd(des->fd_in, des,
			(char(*)(void*, char*, size_t))&ft_des_read_salt,
			FT_READ_ALIGNED | FT_READ_SINGLE_ROUND)) > 0)
			return (ft_ssl_error(opts, "input"));
	}
	if (!opts->opts[DES_OPT_K] || !ret)
		ft_des_pkdf(des, pass, salt);
	else if (!opts->opts[DES_OPT_K] && ret < 0)
		return (ft_ssl_error_spec(opts, "bad magic number"));
	if (des->mode == DES_OPT_E && !opts->opts[DES_OPT_K])
		if (ft_des_print_salt(des))
			return (1);
	ft_des_get_subkeys(des->key, des->skey);
	ft_strdel(&salt);
	if (pass)
		ft_bzero(pass, opts->opts[DES_OPT_P] ? ft_strlen(pass) : _PASSWORD_LEN);
	return (0);
}

char						ft_des_opt_init(t_copt_parsed *opts, t_ft_des *des)
{
	char				*pass;
	char				*salt;

	pass = NULL;
	salt = NULL;
	des->armored = (opts->opts[DES_OPT_A] ? 1 : 0);
	des->ibuf = ft_memalloc(sizeof(t_dbuf));
	dbuf_init(des->ibuf);
	des->obuf = ft_memalloc(sizeof(t_dbuf));
	dbuf_init(des->obuf);
	if (opts->opts[DES_OPT_V])
		des->iv.x64 = ft_pad_hex(copt_get_last_opt(opts, DES_OPT_V).argv[0]);
	if (opts->opts[DES_OPT_K])
		des->key.x64 = ft_pad_hex(copt_get_last_opt(opts, DES_OPT_K).argv[0]);
	else if (!opts->opts[DES_OPT_P] &&
		!(pass = ft_ssl_get_pass(des->mode == DES_OPT_E)))
		return (1);
	else if (opts->opts[DES_OPT_P])
		pass = copt_get_last_opt(opts, DES_OPT_P).argv[0];
	if (opts->opts[DES_OPT_S])
		des->salt.x64 = ft_pad_hex(copt_get_last_opt(opts, DES_OPT_S).argv[0]);
	else if (des->mode == DES_OPT_E && !opts->opts[DES_OPT_K]
		&& !(salt = ft_des_get_salt()))
		return (ft_ssl_error(opts, RANDOM_DEVICE));
	return (ft_des_crypto_init(opts, des, pass, salt));
}

char						ft_des_init(t_copt_parsed *opts, t_ft_des *des)
{
	uint8_t		i;

	i = 0;
	if (ft_base64_init(NULL, &des->base64)
		|| ft_des_opt_init_fd(opts, des)
		|| ft_des_opt_init(opts, des))
		return (1);
	while (ft_strcmp(opts->cmd->text, g_des_block_cipher_mode[i].title))
		i++;
	if (g_des_block_cipher_mode[i].options & DES_IV &&
		opts->opts[DES_OPT_K] && !opts->opts[DES_OPT_V])
		return (ft_fprintf(2, "iv undefined\n") && 1);
	des->cipher_opt = g_des_block_cipher_mode[i].options;
	des->routine = g_des_block_cipher_mode[i].routine;
	return (0);
}

char						ft_des_cb(t_copt_parsed *opts)
{
	char		ret;
	t_ft_des	des;

	ret = 0;
	ft_bzero(&des, sizeof(t_ft_des));
	des.mode = copt_get_mode(opts, DES_OPT_E, DES_OPT_D, DES_OPT_E);
	if (ft_des_init(opts, &des))
	{
		ft_des_destroy(&des);
		return (1);
	}
	if ((ret = ft_read_fd(des.fd_in, &des,
		(char(*)(void*, char*, size_t))&ft_des_io_routine, FT_READ_ALIGNED)))
	{
		ft_des_destroy(&des);
		return (ft_ssl_error(opts, "input"));
	}
	ret = ft_des_finish(&des);
	ft_des_destroy(&des);
	return (ret);
}
