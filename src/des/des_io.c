/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   des_io.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/04 16:02:32 by fle-roy           #+#    #+#             */
/*   Updated: 2019/07/01 14:49:09 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "ft_ssl_des.h"
#include "ft_ssl_base64.h"

char		ft_des_print(t_ft_des *des, uint8_t last)
{
	size_t	len;
	char	ret;

	if (last)
		len = des->obuf->cursor;
	else
		len = (des->obuf->cursor % 8 ? des->obuf->cursor - des->obuf->cursor % 8
			: des->obuf->cursor - 8);
	if (!last && len < 8)
		return (0);
	if (des->armored && des->mode == DES_OPT_E)
		ret = (ft_base64_encode_update(&des->base64, des->obuf->buf, len));
	else
		ret = (write(des->fd_out, des->obuf->buf, len) >= 0
			? 0 : 1);
	dbuf_pop_back(des->obuf, len);
	return (ret);
}

void		ft_des_read_armor(t_ft_des *des, char *buf, size_t len)
{
	if (des->armored && des->mode == DES_OPT_D)
	{
		ft_base64_decode_update(&des->base64, buf, len);
		dbuf_append_w_len(des->ibuf, des->base64.obuf.buf,
			des->base64.obuf.cursor);
		dbuf_clear(&des->base64.obuf);
	}
	else
		dbuf_append_w_len(des->ibuf, buf, len);
}

char		ft_des_read_salt(t_ft_des *des, char *buf, size_t len)
{
	ft_des_read_armor(des, buf, len);
	if (!des->size && des->mode == DES_OPT_D &&
		!ft_memcmp(des->ibuf->buf, SALTED_STRING,
		SALTED_STRING_LEN))
	{
		ft_memrcpy(&des->salt.x64, des->ibuf->buf
			+ SALTED_STRING_LEN, sizeof(uint64_t));
		dbuf_pop_back(des->ibuf, SALTED_STRING_LEN + sizeof(uint64_t));
		return (0);
	}
	return (-1);
}

char		ft_des_io_routine(t_ft_des *des, char *buf, size_t len)
{
	ft_des_read_armor(des, buf, len);
	if (des->ibuf->cursor < READ_BLOCK_AL)
		return (0);
	if ((ft_des_routine(des, 0)))
		return (1);
	return (ft_des_print(des, 0));
}
