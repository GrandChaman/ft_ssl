/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   opts.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fle-roy <francis.leroy@protonmail.ch>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 10:59:11 by fle-roy           #+#    #+#             */
/*   Updated: 2020/02/27 17:06:10 by fle-roy          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ssl.h"
#include "copt.h"
#include "libft.h"

/*
** SHARED OPTIONS - BEGIN
*/

t_copt_opt		g_opt_input = (t_copt_opt)
{
	.sname = 'i',
	.lname = "input",
	.mandatory = 0,
	.argc = 1,
	.validation = NULL,
	.desc = "Input file ('-' for stdin)"
};

t_copt_opt		g_opt_output = (t_copt_opt)
{
	.sname = 'o',
	.lname = "output",
	.mandatory = 0,
	.argc = 1,
	.validation = NULL,
	.desc = "Output file ('-' for stdin)"
};

t_copt_opt		g_opt_encode = (t_copt_opt)
{
	.sname = 'e',
	.lname = "encode",
	.mandatory = 0,
	.argc = 0,
	.validation = NULL,
	.desc = "Encode"
};

t_copt_opt		g_opt_decode = (t_copt_opt)
{
	.sname = 'd',
	.lname = "decode",
	.mandatory = 0,
	.argc = 0,
	.validation = NULL,
	.desc = "Decode"
};

/*
** SHARED OPTIONS - END
*/
/*
** HASH OPTIONS - BEGIN
*/

t_copt_opt		g_gopt_version = (t_copt_opt)
{
	.sname = 'v',
	.lname = "version",
	.mandatory = 0,
	.argc = 0,
	.validation = NULL,
	.desc = "display version"
};

t_copt_opt		g_gopt_help = (t_copt_opt)
{
	.sname = 'h',
	.lname = "help",
	.mandatory = 0,
	.argc = 0,
	.validation = NULL,
	.desc = "display help"
};

t_copt_opt		g_opt_print = (t_copt_opt)
{
	.sname = 'p',
	.lname = "print",
	.mandatory = 0,
	.argc = 0,
	.validation = NULL,
	.desc = "echo stdin to stdout"
};
t_copt_opt		g_opt_quiet = (t_copt_opt)
{
	.sname = 'q',
	.lname = "quiet",
	.mandatory = 0,
	.argc = 0,
	.validation = NULL,
	.desc = "quiet mode"
};
t_copt_opt		g_opt_reverse = (t_copt_opt)
{
	.sname = 'r',
	.lname = "reverse",
	.mandatory = 0,
	.argc = 0,
	.validation = NULL,
	.desc = "reverse output format"
};
t_copt_opt		g_opt_string = (t_copt_opt)
{
	.sname = 's',
	.lname = "string",
	.mandatory = 0,
	.argc = 1,
	.validation = NULL,
	.desc = "print the sum of the given string"
};

/*
** HASH OPTIONS - END
*/
/*
** BASE64 OPTIONS - BEGIN
*/

t_copt_opt		g_opt_base64_break = (t_copt_opt)
{
	.sname = 'b',
	.lname = "break",
	.mandatory = 0,
	.argc = 1,
	.validation = NULL,
	.desc = "Line width (0 for unbroken)"
};

/*
** BASE64 OPTIONS - END
*/
/*
** DES OPTIONS - BEGIN
*/

t_copt_opt		g_opt_des_base64 = (t_copt_opt)
{
	.sname = 'a',
	.lname = "armor",
	.mandatory = 0,
	.argc = 0,
	.validation = NULL,
	.desc = "Armor/Dearmor with base64 depending on encrypt mode"
};

t_copt_opt		g_opt_des_key = (t_copt_opt)
{
	.sname = 'k',
	.lname = "key",
	.mandatory = 0,
	.argc = 1,
	.validation = &ft_des_check_hex,
	.desc = "Key in hex"
};

t_copt_opt		g_opt_des_pass = (t_copt_opt)
{
	.sname = 'p',
	.lname = "password",
	.mandatory = 0,
	.argc = 1,
	.validation = NULL,
	.desc = "Specify password"
};

t_copt_opt		g_opt_des_salt = (t_copt_opt)
{
	.sname = 's',
	.lname = "salt",
	.mandatory = 0,
	.argc = 1,
	.validation = &ft_des_check_hex,
	.desc = "Specify salt in hex"
};

t_copt_opt		g_opt_des_iv = (t_copt_opt)
{
	.sname = 'v',
	.lname = "ivector",
	.mandatory = 0,
	.argc = 1,
	.validation = &ft_des_check_hex,
	.desc = "Specify IV in hex"
};

/*
** DES OPTIONS - END
*/
/*
** HASH CMD - BEGIN
*/

t_copt_cmd		g_cmd_md5 = (t_copt_cmd)
{
	.text = "md5",
	.cb = &ft_hash_select,
	.opt = (t_copt_opt*[]){&g_opt_print, &g_opt_quiet, &g_opt_reverse,
		&g_opt_string, NULL},
	.desc = NULL
};
t_copt_cmd		g_cmd_sha1 = (t_copt_cmd)
{
	.text = "sha1",
	.cb = &ft_hash_select,
	.opt = (t_copt_opt*[]){&g_opt_print, &g_opt_quiet, &g_opt_reverse,
		&g_opt_string, NULL},
	.desc = NULL
};
t_copt_cmd		g_cmd_sha224 = (t_copt_cmd)
{
	.text = "sha224",
	.cb = &ft_hash_select,
	.opt = (t_copt_opt*[]){&g_opt_print, &g_opt_quiet, &g_opt_reverse,
		&g_opt_string, NULL},
	.desc = NULL
};
t_copt_cmd		g_cmd_sha256 = (t_copt_cmd)
{
	.text = "sha256",
	.cb = &ft_hash_select,
	.opt = (t_copt_opt*[]){&g_opt_print, &g_opt_quiet, &g_opt_reverse,
		&g_opt_string, NULL},
	.desc = NULL
};
t_copt_cmd		g_cmd_sha384 = (t_copt_cmd)
{
	.text = "sha384",
	.cb = &ft_hash_select,
	.opt = (t_copt_opt*[]){&g_opt_print, &g_opt_quiet, &g_opt_reverse,
		&g_opt_string, NULL},
	.desc = NULL
};
t_copt_cmd		g_cmd_sha512 = (t_copt_cmd)
{
	.text = "sha512",
	.cb = &ft_hash_select,
	.opt = (t_copt_opt*[]){&g_opt_print, &g_opt_quiet, &g_opt_reverse,
		&g_opt_string, NULL},
	.desc = NULL
};
t_copt_cmd		g_cmd_sha512224 = (t_copt_cmd)
{
	.text = "sha512224",
	.cb = &ft_hash_select,
	.opt = (t_copt_opt*[]){&g_opt_print, &g_opt_quiet, &g_opt_reverse,
		&g_opt_string, NULL},
	.desc = NULL
};
t_copt_cmd		g_cmd_sha512256 = (t_copt_cmd)
{
	.text = "sha512256",
	.cb = &ft_hash_select,
	.opt = (t_copt_opt*[]){&g_opt_print, &g_opt_quiet, &g_opt_reverse,
		&g_opt_string, NULL},
	.desc = NULL
};

/*
** HASH CMD - END
*/
/*
** BASE64 CMD - BEGIN
*/

t_copt_cmd		g_cmd_base64 = (t_copt_cmd)
{
	.text = "base64",
	.cb = &ft_base64_cb,
	.opt = (t_copt_opt*[]){&g_opt_encode, &g_opt_decode, &g_opt_input,
		&g_opt_output, &g_opt_base64_break, NULL},
	.desc = NULL
};

/*
** BASE64 CMD - END
*/
/*
** DES CMD - BEGIN
*/

t_copt_cmd		g_cmd_des_ecb = (t_copt_cmd)
{
	.text = "des-ecb",
	.cb = &ft_des_cb,
	.opt = (t_copt_opt*[]){&g_opt_encode, &g_opt_decode, &g_opt_input,
		&g_opt_output, &g_opt_base64_break,
		&g_opt_des_base64, &g_opt_des_key, &g_opt_des_pass, &g_opt_des_salt,
		&g_opt_des_iv, NULL},
	.desc = NULL
};

t_copt_cmd		g_cmd_des = (t_copt_cmd)
{
	.text = "des",
	.cb = &ft_des_cb,
	.opt = (t_copt_opt*[]){&g_opt_encode, &g_opt_decode, &g_opt_input,
		&g_opt_output, &g_opt_base64_break, &g_opt_des_base64, &g_opt_des_key,
		&g_opt_des_pass, &g_opt_des_salt, &g_opt_des_iv, NULL},
	.desc = "Same as des-cbc"
};

t_copt_cmd		g_cmd_des_cbc = (t_copt_cmd)
{
	.text = "des-cbc",
	.cb = &ft_des_cb,
	.opt = (t_copt_opt*[]){&g_opt_encode, &g_opt_decode, &g_opt_input,
		&g_opt_output, &g_opt_base64_break, &g_opt_des_base64, &g_opt_des_key,
		&g_opt_des_pass, &g_opt_des_salt, &g_opt_des_iv, NULL},
	.desc = NULL
};

t_copt_cmd		g_cmd_des_ofb = (t_copt_cmd)
{
	.text = "des-ofb",
	.cb = &ft_des_cb,
	.opt = (t_copt_opt*[]){&g_opt_encode, &g_opt_decode, &g_opt_input,
		&g_opt_output, &g_opt_base64_break, &g_opt_des_base64, &g_opt_des_key,
		&g_opt_des_pass, &g_opt_des_salt, &g_opt_des_iv, NULL},
	.desc = NULL
};

t_copt_cmd		g_cmd_des_cfb = (t_copt_cmd)
{
	.text = "des-cfb",
	.cb = &ft_des_cb,
	.opt = (t_copt_opt*[]){&g_opt_encode, &g_opt_decode, &g_opt_input,
		&g_opt_output, &g_opt_base64_break, &g_opt_des_base64, &g_opt_des_key,
		&g_opt_des_pass, &g_opt_des_salt, &g_opt_des_iv, NULL},
	.desc = NULL
};

/*
** DES CMD - END
*/

t_copt_cmd		g_gcmd = (t_copt_cmd)
{
	.text = NULL,
	.cb = &ft_ssl_global_cmd,
	.opt = (t_copt_opt*[]){&g_gopt_version, &g_gopt_help, NULL},
	.desc = NULL
};
t_copt			g_copt = (t_copt)
{
	.prgm_name = "ft_ssl",
	.version = VERSION,
	.cmd = (t_copt_cmd*[]){&g_gcmd, &g_cmd_md5, &g_cmd_sha1, &g_cmd_sha224,
		&g_cmd_sha256, &g_cmd_sha384, &g_cmd_sha512,
		&g_cmd_sha512224, &g_cmd_sha512256, &g_cmd_base64, &g_cmd_des,
		&g_cmd_des_ecb, &g_cmd_des_cbc, &g_cmd_des_ofb, &g_cmd_des_cfb, NULL},
	.print = &ft_printf,
	.desc = NULL
};

char			ft_ssl_global_cmd(t_copt_parsed *parsed)
{
	if (parsed->gopts && parsed->gopts[GB_OPT_V])
		ft_printf("%s\n", VERSION);
	else if (parsed->gopts && parsed->gopts[GB_OPT_H])
		copt_display_help(g_copt);
	else
	{
		if (parsed->params && parsed->params->argc && parsed->params->argv)
			ft_fprintf(2, "Command %s not found\n", parsed->params->argv[0]);
		else if (!parsed->cmd)
			ft_fprintf(2, "No command provided\n");
		else
			return (0);
		copt_display_help(g_copt);
		return (1);
	}
	return (0);
}
